# Prerequisites

This version is known to compile with:

 - Coq 8.6.1
 - Ssreflect 1.6
 - Autosubst branch [coq86-devel](https://github.com/uds-psl/autosubst/tree/coq86-devel)
 - std++ version [ca0be4274dd7593c44db46670d078095e9f6c1c7](https://gitlab.mpi-sws.org/robbertkrebbers/coq-stdpp/tree/ca0be4274dd7593c44db46670d078095e9f6c1c7)
 - Iris version [bfdd67a736ad91c3838bffde3f57af516dec56d8](https://gitlab.mpi-sws.org/FP/iris-coq/tree/bfdd67a736ad91c3838bffde3f57af516dec56d8)

# Compilation

Make sure that all the dependencies are installed and run `make`.

# Documentation

See [refman.md](docs/refman.md).

Work in progress.

# Structure

- `logrel.v` - exports all the modules required to perform refinement proofs such as those in the `example/` folder
- `F_mu_ref_conc` - definition of the object language
- `examples` - example refinements
- `logrel` contains modules related to the relational interpretation
  + `threadpool.v` - definitions for the ghost threadpool RA
  + `rules_threadpool.v` - symbolic execution in the threadpool
  + `proofmode/tactics_threadpool.v` - tactics for preforming symbolic execution in the threadpool
  + `semtypes.v` - interpretation of types/semantics in terms of values
  + `logrel_binary.v` - interpretation of sequents/semantics in terms of open expressions
  + `rules.v` - symbolic execution rules for the relational interpretation
  + `proofmode/tactics_rel.v` - tactics for performing symbolic execution in the relational interpretation
  + `fundamental_binary.v` - compatibility lemmas and the fundamental theorem of logical relations
  + `contextual_refinement.v` - proof that logical relations are closed under contextual refinement
  + `soundness_binary.v` - typesafety and contextual refinement proofs for terms in the relational interpretation  
- `prelude` - some files shares by the whole development
