From iris.program_logic Require Export ectx_language ectxi_language.
From iris.algebra Require Export ofe.
From iris_logrel.F_mu_ref_conc Require Export lang.

(** Definitions and properties about substitution and closedness *)

(** * Definitions *)
(** Parallel substitution *)
Fixpoint subst_p (es : stringmap val) (e : expr) : expr :=
  match e with
  | Var y => match es !! y with
             | None => Var y
             | Some e' => of_val e'
             end
  | Rec f y e =>
    Rec f y $ subst_p (delete f (delete y es)) e
  | App e1 e2 => App (subst_p es e1) (subst_p es e2)
  | TLam e => TLam (subst_p es e)
  | TApp e => TApp (subst_p es e)
  | Lit l => Lit l
  | BinOp op e1 e2 => BinOp op (subst_p es e1) (subst_p es e2)
  | If e0 e1 e2 => If (subst_p es e0) (subst_p es e1) (subst_p es e2)
  | Pair e1 e2 => Pair (subst_p es e1) (subst_p es e2)
  | Fst e => Fst (subst_p es e)
  | Snd e => Snd (subst_p es e)
  | Fold e => Fold (subst_p es e)
  | Unfold e => Unfold (subst_p es e)
  | Pack e => Pack (subst_p es e)                
  | Unpack e0 e => 
    Unpack (subst_p es e0) (subst_p es e)
  | InjL e => InjL (subst_p es e)
  | InjR e => InjR (subst_p es e)
  | Case e0 e1 e2 => 
    Case (subst_p es e0)
         (subst_p es e1)
         (subst_p es e2)
  | Fork e => Fork (subst_p es e)
  | Alloc e => Alloc (subst_p es e)
  | Load e => Load (subst_p es e)
  | Store e1 e2 => Store (subst_p es e1) (subst_p es e2)
  | CAS e0 e1 e2 => CAS (subst_p es e0) (subst_p es e1) (subst_p es e2)
  end.

(** Substitution in the contexts *)
Fixpoint subst_ctx_item (es : stringmap val) (K : ectx_item)
         {struct K} :=
  match K with
  | AppLCtx e2 => AppLCtx (subst_p es e2)
  | AppRCtx v1 => AppRCtx v1
  | TAppCtx => TAppCtx
  | PairLCtx e2 => PairLCtx (subst_p es e2)
  | PairRCtx v1 => PairRCtx v1
  | BinOpLCtx op e2 => BinOpLCtx op (subst_p es e2)
  | BinOpRCtx op v1 => BinOpRCtx op v1
  | FstCtx => FstCtx
  | SndCtx => SndCtx
  | InjLCtx => InjLCtx
  | InjRCtx => InjRCtx
  | CaseCtx e1 e2 => 
    CaseCtx (subst_p es e1) (subst_p es e2)
  | IfCtx e1 e2 => IfCtx (subst_p es e1) (subst_p es e2)
  | FoldCtx => FoldCtx
  | UnfoldCtx => UnfoldCtx
  | PackCtx => PackCtx
  | UnpackLCtx e2 => UnpackLCtx (subst_p es e2)
  | AllocCtx => AllocCtx
  | LoadCtx => LoadCtx
  | StoreLCtx e2 => StoreLCtx (subst_p es e2)
  | StoreRCtx v1 => StoreRCtx v1
  | CasLCtx e1 e2 => CasLCtx (subst_p es e1) (subst_p es e2)
  | CasMCtx v0 e2 => CasMCtx v0 (subst_p es e2)
  | CasRCtx v0 v1 => CasRCtx v0 v1
  end.

Definition subst_ctx (es : stringmap val) (K : list ectx_item) :=
  map (subst_ctx_item es) K.


(** * Properties *)
(** ** Parallel substitution properties *)
Lemma subst_p_empty e :
    subst_p ∅ e = e.
Proof.
  induction e; simpl; simplify_option_eq; auto 10 with f_equal.
  destruct f as [|f], x as [|x]; cbn; 
    rewrite ?delete_empty; auto 10 with f_equal.
Qed.

Lemma subst_p_singleton x es e :
  subst_p {[x:=es]} e = subst x (of_val es) e.
Proof.
  induction e; simpl; simplify_option_eq; auto 10 with f_equal.
  - by rewrite lookup_singleton.
  - by rewrite lookup_singleton_ne.
  - destruct H as [H1 H2].
    destruct f as [|f], x0 as [|y]; cbn; auto 10 with f_equal;
    repeat lazymatch goal with
    | [ H : BNamed ?x ≠ BNamed ?y |- _ ] =>
      assert (x ≠ y) by exact (λ X, H (f_equal _ X)); clear H
    end;
    rewrite ?delete_singleton_ne;
    auto 10 with f_equal.
  - apply not_and_l in H as [<-%dec_stable|<-%dec_stable].
    + rewrite delete_commute_binder. cbn.
      rewrite delete_singleton. destruct x0; cbn; rewrite ?delete_empty;
      by rewrite subst_p_empty.
    + cbn. rewrite delete_singleton.
      destruct f; cbn; rewrite ?delete_empty;
      by rewrite subst_p_empty.
Qed.

(** ** Properties of [Closed] and [is_closed] *)
Lemma is_closed_weaken (e : expr) : ∀ (X Y : stringset),
    is_closed X e → X ⊆ Y → is_closed Y e.
Proof. 
  induction e; intros X Y HXY HXe; simpl; simpl in HXe, HXY; eauto;
    try set_solver.
  { eapply IHe. eassumption. set_solver. }
Qed.

Lemma Closed_mono e : ∀ X Y, Closed X e → X ⊆ Y →  Closed Y e.
Proof. 
  rewrite /Closed.
  apply is_closed_weaken.
Qed.

Lemma of_val_closed' (v : val) X :
  Closed X (of_val v).
Proof.
  induction v; simpl; rewrite /Closed; simpl; eauto.
  apply (Closed_mono _ (x :b: f :b: ∅)); auto. set_solver.
  apply (Closed_mono _ ∅); auto. set_solver.
Qed.

Lemma of_val_closed (v : val) :
  Closed ∅ (of_val v).
Proof.
  apply of_val_closed'.
Qed.

Lemma to_val_closed (e : expr) (v : val) :
  to_val e = Some v →
  Closed ∅ e.
Proof.
  intros Hv. rewrite -(of_to_val _ _ Hv).
  apply of_val_closed.
Qed.

Hint Resolve to_val_closed.
Hint Resolve of_val_closed.
(* I find this mildly useful for this file *)
Local Hint Extern 0 (Closed _ _) => eauto : typeclass_instances.

(** *** Relations between [Closed] and substitutions *)
(** Closure properties of [Closed] *)
Lemma subst_closed x (es : expr) e `{Closed X e} `{Closed X es}  :
  Closed X (subst x es e).
Proof.
  generalize dependent X.
  induction e; intros X HXe HXes; simpl;
  try case_decide; eauto;
  rewrite /Closed /=; rewrite /Closed /= in HXe; eauto;
  repeat lazymatch goal with
  | [ H : Is_true (?P && ?Q) |- _ ] => apply andb_prop_elim in H; destruct H; eauto
  | [ |- Is_true (?P && ?Q) ] => apply andb_prop_intro; split; eauto
         end;
  try match goal with
  | [ H : ∀ X : stringset,
        Closed X ?e → Closed X es → Closed X (subst x es ?e) 
        |- Is_true (is_closed _ (subst x es ?e)) ] =>
    eapply H; eauto; try (eapply Closed_mono; eauto; set_solver)
  end.
Qed.

Lemma is_closed_subst_preserve X e x es :
  is_closed ∅ es → is_closed ({[x]} ∪ X) e → is_closed X (subst x es e).
Proof.
  intros ?. revert X.
  induction e=> X /= ?; destruct_and?; split_and?; simplify_option_eq;
  try match goal with
  | H : ¬(_ ∧ _) |- _ => apply not_and_l in H as [?%dec_stable|?%dec_stable]
      end; eauto using is_closed_weaken with set_solver.
Qed.

Lemma subst_p_delete_Closed : forall e X (x:string) vs,
  Closed X (subst_p vs e) →
  Closed ({[x]} ∪ X) (subst_p (delete x vs) e).
Proof.
  rewrite /Closed.
  induction e; intros X s vs Hcl;
   try (split_and?; naive_solver).
  - simpl. simpl in Hcl. 
    remember (delete s vs !! x) as Hx0.
    destruct Hx0.
    + apply of_val_closed'.
    + simpl. destruct (decide (x = s)) as [e|ne]; subst.
      * apply bool_decide_pack. set_solver.
      * case_match.
        { exfalso. symmetry in HeqHx0.
          rewrite lookup_delete_ne in HeqHx0; auto.
          naive_solver. }
        { apply bool_decide_pack.
          simpl in Hcl. apply bool_decide_spec in Hcl.
          set_solver. }
  - simpl.
    destruct f as [|f], x as [|x]; cbn-[union]; eauto; cbn-[union] in Hcl.
    + rewrite delete_commute.
      rewrite assoc (comm _ {[x]} {[s]}) -assoc.
      by eapply IHe.
    + rewrite delete_commute.
      rewrite assoc (comm _ {[f]} {[s]}) -assoc.
      by eapply IHe.
    + rewrite !(delete_commute _ _ s).
      rewrite (assoc _ {[f]} {[s]}).
      rewrite (comm _ {[f]} {[s]}).
      rewrite !assoc.
      rewrite (comm union {[x]} {[s]}).
      rewrite -!assoc.
      repeat eapply IHe. eauto.
Qed.

Lemma subst_p_closes X Z vs e :
  Closed X e →
  X ⊆ dom _ vs ∪ Z →
  Closed Z (subst_p vs e).
Proof.
  generalize dependent vs. 
  generalize dependent Z.
  generalize dependent X. 
  rewrite /Closed.
  induction e; intros X Z vs Hcl Hdom;
   try (simpl; split_and?; naive_solver).
  { (* variable case *)
    simpl. case_match.
    - apply of_val_closed'.
    - simpl in Hcl. apply bool_decide_spec in Hcl.
      cbn. apply bool_decide_spec.
      assert (x ∈ dom stringset vs ∪ Z) as HxvsZ.
      { set_solver. }
      assert (x ∉ dom stringset vs) as Hnvs.
      { intros Hx. apply elem_of_dom in Hx. destruct Hx; naive_solver. }
      set_solver.
  }
  { (* lambda case *)
    simpl. destruct f as [|f], x as [|x]; cbn-[union]; eauto.
    + simpl in Hcl.
      eapply IHe; eauto.
      rewrite dom_delete.
      (* Set solver cannot solve this because we have an additional assumption of decidable equality on [X] *)
      revert Hdom. clear.
      set_unfold. intros Hdom.
      intros y [Hy|Hy]; subst; eauto.
      destruct (Hdom _ Hy); eauto.
      destruct (decide (y = x)); subst; eauto.
    + simpl in Hcl.
      eapply IHe; eauto.
      rewrite dom_delete.
      set_unfold.
      intros y [Hy|Hy]; subst; eauto.
      destruct (Hdom _ Hy); eauto.
      destruct (decide (y = f)); subst; eauto.
    + simpl in Hcl.
      eapply IHe; eauto.
      rewrite delete_commute.
      rewrite !dom_delete.
      set_unfold.
      intros y [Hx|[Hf|Hy]]; subst; eauto.
      destruct (Hdom _ Hy); eauto.
      destruct (decide (y = f)); subst; eauto.
      destruct (decide (y = x)); subst; eauto.
  }
Qed.

(** When substitutions are identities *)
Lemma is_closed_subst_id X e x es :
  is_closed X e → x ∉ X → subst x es e = e.
Proof.
  revert X. induction e=> X /=; rewrite ?bool_decide_spec ?andb_True=> ??;
  repeat case_decide; simplify_eq/=; f_equal; intuition eauto with set_solver.
Qed.

Lemma Closed_subst_id e x es :
  Closed ∅ e →
  subst x es e = e.
Proof. intros Hc. eapply is_closed_subst_id. apply Hc. set_solver. Qed.

Lemma Closed_subst_p_id' X es e :
  Closed X e →
  (∀ x, x ∈ X → es !! x = None) →
  subst_p es e = e.
Proof.
  revert X es; unfold Closed. induction e => X es; simplify_option_eq;
  rewrite ?bool_decide_spec; move => Hc HX; intuition eauto 20 with set_solver;
  repeat lazymatch goal with
  | [IH : context[subst_p _ ?e = ?e] |- _ ] =>
    erewrite IH; eauto; clear IH
  end. all: try set_solver.
  - specialize (HX x). by rewrite HX.
  - intro y. destruct f as [|f], x as [|x]; simpl; cbn-[union]; eauto;
    rewrite ?elem_of_union ?elem_of_singleton;
    intros [H | H]; rewrite ?lookup_delete_None; eauto.
    destruct H; eauto.
Qed.

Lemma Closed_subst_p_id es e :
  Closed ∅ e →
  subst_p es e = e.
Proof. 
  intros. eapply Closed_subst_p_id'; eauto.
  intro x; rewrite elem_of_empty; intuition.
Qed.

Lemma of_val_subst (v : val) x (es : expr) :
  subst x es (of_val v) = of_val v.
Proof.
  by apply Closed_subst_id.
Qed.

Lemma of_val_subst_p (v : val) (es : stringmap val) :
  subst_p es (of_val v) = of_val v.
Proof.
  by apply Closed_subst_p_id.
Qed.

Lemma to_val_subst_p (e : expr) (v : val) (es : stringmap val) :
  to_val e = Some v → subst_p es e = e.
Proof.
  intros Htv.
  rewrite -(of_to_val _ _ Htv).
  apply of_val_subst_p.
Qed.

(** Relation between [subst_p] and [subst] *)
Lemma subst_p_insert (x : binder) v (m : stringmap val) e :
  subst_p (<[x:=v]>m) e =
  (subst_p m (subst' x (of_val v) e)).
Proof. 
  generalize dependent m.
  destruct x as [|x]; cbn; auto.
  induction e; simpl; intro m; auto 10 with f_equal;
  repeat match goal with
  | [ IH : subst_p _ _ = subst' _ _ _  |- _ ] => 
  rewrite IH; clear IH
  end.
  - simplify_option_eq.
    + rewrite lookup_insert. by rewrite Closed_subst_p_id.
    + by rewrite lookup_insert_ne.
  - f_equal. simplify_option_eq.
    + destruct_and!. 
      rewrite !delete_insert_ne_binder' //.
    + apply not_and_l in H as [<-%dec_stable|<-%dec_stable].
      * rewrite delete_commute_binder.
        cbn[delete delete_binder].
        rewrite delete_insert_delete.
        rewrite (delete_commute_binder _ x0 (BNamed x)).
        reflexivity.
      * cbn[delete delete_binder].
        rewrite delete_insert_delete.
        reflexivity.
Qed.
  
Lemma subst_p_delete (x : binder) (e' : expr) (m : stringmap val) e `{Closed ∅ e'} :
  subst_p m (subst' x e' e) =
  subst' x e' (subst_p (delete x m) e).
Proof.
  generalize dependent m.
  destruct x as [|x]; cbn; auto.
  induction e; simpl; intro m; auto 10 with f_equal;
  repeat match goal with
  | [ IH : subst_p _ _ = subst' _ _ _  |- _ ] => 
    rewrite IH; clear IH
  end.
  - simplify_option_eq.
    + rewrite lookup_delete. 
      rewrite Closed_subst_p_id. simpl. by rewrite decide_left.
    + rewrite lookup_delete_ne; auto.
      case_match.
      * by rewrite Closed_subst_id.
      * simpl. by rewrite decide_False.
  - f_equal. simplify_option_eq.
    + destruct_and!. 
      rewrite (delete_commute_binder _ x0 (BNamed x)).
      rewrite IHe. 
      by rewrite (delete_commute_binder _ f (BNamed x)).
    + apply not_and_l in H0 as [<-%dec_stable|<-%dec_stable].
      * rewrite (delete_commute_binder _ x0 (BNamed x)).
        by rewrite delete_idemp_binder.
      * by rewrite delete_idemp_binder.
Qed.
  
Lemma subst_p_rec (x1 x2 : binder) v1 v2 e :
  subst_p (<[x1:=v1]>{[x2:=v2]}) e = subst' x2 (of_val v2) (subst' x1 (of_val v1) e).
Proof.
  rewrite subst_p_insert.
  replace {[x2 := v2]} with (<[x2 := v2]>(∅ : stringmap val)); last first.
  { by destruct x2; cbn. }
  rewrite subst_p_insert.
  by rewrite subst_p_empty.
Qed.

Lemma elem_of_rec_lookup (x f : binder) (es : stringmap val) :
  ∀ z : string, z ∈ x :b: f :b: ∅ → delete x (delete f es) !! z = None.
Proof.
  intros z.
  destruct x as [|x],f as [|f]; cbn-[union singleton];
    [ rewrite elem_of_empty; inversion 1
    | rewrite right_id ?elem_of_union !elem_of_singleton.. ].
  * intros; subst.
    apply lookup_delete.
  * intros; subst.
    apply lookup_delete.
  * intros [[] | []];
      [ apply lookup_delete | rewrite delete_commute ; apply lookup_delete ].
Qed.

(** Properties of the context substitution *)
Lemma fill_item_subst_p (es : stringmap val) (Ki : ectx_item) (e : expr)  :
  subst_p es (fill_item Ki e) = fill_item (subst_ctx_item es Ki) (subst_p es e).
Proof.
  induction Ki; simpl;
  rewrite ?of_val_subst_p; eauto.
Qed.

Lemma fill_subst (es : stringmap val) (K : list ectx_item) (e : expr) :
  subst_p es (fill K e) = fill (subst_ctx es K) (subst_p es e).
Proof.
  generalize dependent e. generalize dependent es.
  induction K as [|Ki K]; eauto.
  intros es e. simpl.
  rewrite IHK.
    by rewrite ?fill_item_subst_p.
Qed.

(** Substitutions and reductions *)

Lemma subst_p_head_step e e' vs efs σ σ' :
  head_step e σ e' σ' efs →
  head_step (subst_p vs e) σ (subst_p vs e') σ' (subst_p vs <$> efs).
Proof.
  intros Hst.
  generalize dependent vs.
  inversion Hst; subst; simpl;
  intros vs;
  repeat lazymatch goal with
  | [ H : to_val ?e = Some ?v |- _ ] =>
    rewrite -!(of_to_val e v H);
    clear H
  end;
  rewrite ?of_val_subst_p;
  try by (econstructor; eauto using to_of_val).
  - econstructor; eauto using to_of_val.
    + eapply subst_p_closes; eauto.
      set_solver.
    + rewrite !subst_p_delete.
      rewrite !(delete_commute_binder _ f x).
      rewrite (Closed_subst_p_id' _ _ e1); eauto.
      apply elem_of_rec_lookup.
  - econstructor. by rewrite Closed_subst_p_id.
Qed.

Lemma subst_p_prim_step e e' vs efs σ σ' :
  prim_step e σ e' σ' efs →
  prim_step (subst_p vs e) σ (subst_p vs e') σ' (subst_p vs <$> efs).
Proof.
  intros Hst.
  destruct Hst as [K e1 e2 ?? Hst]; subst.
  rewrite !fill_subst.
  eapply Ectx_step with (subst_ctx vs K) (subst_p vs e1) (subst_p vs e2); eauto.
  simpl. by apply subst_p_head_step.
Qed.

Lemma subst_p_safe_head e es :
  (∀ σ, head_reducible e σ) → (∀ σ, head_reducible (subst_p es e) σ).
Proof.
  rewrite /head_reducible.
  intros Hsafe.
  intros σ. destruct (Hsafe σ) as (e' & σ' & efs & Hstep).
  do 3 eexists. apply subst_p_head_step; eauto.
Qed.

Lemma subst_p_safe e es :
  (∀ σ, reducible e σ) → (∀ σ, reducible (subst_p es e) σ).
Proof.
  rewrite /reducible.
  intros Hsafe.
  intros σ. destruct (Hsafe σ) as (e' & σ' & efs & Hstep).
  destruct Hstep as [K ? ? ? ? Hstep]. subst.
  do 3 eexists. rewrite fill_subst.
  eapply Ectx_step; eauto.
  apply subst_p_head_step; eauto.
Qed.

Lemma subst_p_det_head e e' es σ1 σ2 efs :
  head_step e σ1 e' σ1 [] →
  (∀ e2, head_step e σ1 e2 σ2 efs -> σ1=σ2 /\ e'=e2 /\ [] = efs) →
  (∀ e2, head_step (subst_p es e) σ1 e2 σ2 efs -> σ1=σ2 /\ (subst_p es e')=e2 /\ [] = efs).
Proof.
  intros Hstep Hdet e2 Hst.
  inversion Hstep; subst; simplify_eq/=;
    simpl in Hst; inversion Hst; simplify_eq/=; eauto;
  repeat lazymatch goal with
  | [ H : to_val ?e = Some ?v, H' : to_val (subst_p es ?e) = Some ?v2 |- _ ] =>
    rewrite -(of_to_val _ _ H) in H';
    rewrite of_val_subst_p in H';
    rewrite to_of_val in H'
  | [ H : to_val ?e = Some ?v, H' : context[subst_p _ ?e] |- _ ] =>
    rewrite -(of_to_val _ _ H) in H';
    rewrite of_val_subst_p in H';
    rewrite (of_to_val _ _ H) in H'
  end; simplify_eq/=;
  rewrite ?of_val_subst_p;
  split_and !; eauto.
  - rewrite !subst_p_delete.
    rewrite !(delete_commute_binder _ f x).
    rewrite (Closed_subst_p_id' _ _ e1); eauto.
    rewrite (to_val_subst_p e0 v0 _); eauto.
    apply elem_of_rec_lookup.
  - destruct (Hdet _ Hst) as (?&?&?).
    by simplify_eq.
  - destruct (Hdet _ Hst) as (?&?&?).
    by simplify_eq.
  - destruct (Hdet _ Hst) as (?&?&?).
    by simplify_eq.
Qed.

Lemma subst_p_det e e' es :
  (∀ σ, prim_step e σ e' σ []) →
  (∀ σ1 e2 σ2 efs, prim_step e σ1 e2 σ2 efs -> σ1=σ2 /\ e'=e2 /\ [] = efs) →
  (∀ σ1 e2 σ2 efs, prim_step (subst_p es e) σ1 e2 σ2 efs -> σ1=σ2 /\ (subst_p es e')=e2 /\ [] = efs).
Proof.
  intros Hstep Hdet σ1 e2 σ2 efs Hsstep.
  destruct Hsstep as [K e1' e2' ???]; simplify_eq/=.
  destruct (Hstep σ1) as [K' e1'' e2'' ???]; simplify_eq/=.
  rewrite fill_subst in H.
  assert (K = (subst_ctx es K')) as ->.
  { eapply (subst_p_head_step _ _ es) in H3.
    edestruct (step_by_val K (subst_ctx es K') e1' (subst_p es e1''))
      as [K1 ?]; eauto using val_stuck.
    edestruct (step_by_val (subst_ctx es K') K (subst_p es e1'') e1')
      as [[|??] ->]; eauto using val_stuck; discriminate_list. }
  apply fill_inj in H; subst.
  rewrite fill_subst.
  cut (σ1 = σ2 ∧ subst_p es e2'' = e2' ∧ [] = efs).
  { naive_solver. }
  eapply subst_p_det_head. eauto. 2: eauto.
  intros e2 ?.
  destruct (Hdet σ1 (fill K' e2) σ2 efs) as (?&?%fill_inj&?).
  { by econstructor. }
  done.
Qed.
