From iris.algebra Require Import ofe.
From iris.program_logic Require Export language.
From iris.program_logic Require Export ectx_language ectxi_language.
From iris_logrel.prelude Require Export base.
From iris_logrel.F_mu_ref_conc Require Export binder.

Module lang.
  (** * Syntax and syntactic categories *)
  Definition loc := positive.

  Instance loc_dec_eq : EqDecision loc := _.

  (** ** Expressions *)
  Inductive binop := Add | Sub | Eq | Le | Lt | Xor.
  
  Instance binop_dec_eq : EqDecision binop.
  Proof. solve_decision. Defined.

  Inductive literal := Unit | Nat (n : nat) | Bool (b : bool) | Loc (l : loc).

  Inductive expr :=
  | Var (x : string)
  (* λ-rec *)
  | Rec (f x : binder) (e : expr)
  | App (e1 e2 : expr)
  (* Constants *)
  | Lit (l : literal)
  | BinOp (op : binop) (e1 e2 : expr)
  (* If then else *)
  | If (e0 e1 e2 : expr)
  (* Products *)
  | Pair (e1 e2 : expr)
  | Fst (e : expr)
  | Snd (e : expr)
  (* Sums *)
  | InjL (e : expr)
  | InjR (e : expr)
  | Case (e0 : expr) (e1 : expr) (e2 : expr)
  (* Recursive Types *)
  | Fold (e : expr)
  | Unfold (e : expr)
  (* Polymorphic Types *)
  | TLam (e : expr)
  | TApp (e : expr)
  (* Existential types *)
  | Pack (e : expr)
  | Unpack (e : expr) (e' : expr)
  (* Concurrency *)
  | Fork (e : expr)
  (* Reference Types *)
  | Alloc (e : expr)
  | Load (e : expr)
  | Store (e1 : expr) (e2 : expr)
  (* Compare and swap used for fine-grained concurrency *)
  | CAS (e0 : expr) (e1 : expr) (e2 : expr).
  Bind Scope expr_scope with expr.

  Instance literal_dec_eq : EqDecision literal.
  Proof. solve_decision. Defined.

  Instance expr_dec_eq : EqDecision expr.
  Proof. solve_decision. Defined.

  (** ** Closed expressions *)
  Fixpoint is_closed (X : stringset) (e : expr) : bool :=
    match e with
    | Var x => bool_decide (x ∈ X)
    | Rec f x e => is_closed (x :b: f :b: X) e
    | Lit _ => true
    | Fst e | Snd e | InjL e | InjR e | Fork e | Alloc e | Load e | Fold e | Unfold e | TLam e | TApp e | Pack e => 
      is_closed X e
    | App e1 e2 | BinOp _ e1 e2 | Pair e1 e2 | Store e1 e2 =>
      is_closed X e1 && is_closed X e2
    | If e0 e1 e2 | CAS e0 e1 e2 =>
      is_closed X e0 && is_closed X e1 && is_closed X e2
    | Case e0 e1 e2 =>
      is_closed X e0 && is_closed X e1 
                     && is_closed X e2      
    | Unpack e e1 => is_closed X e && is_closed X e1
    end.

  Class Closed (X : stringset) (e : expr) := closed : is_closed X e.
  Instance closed_proof_irrel env e : ProofIrrel (Closed env e).
  Proof. rewrite /Closed. apply _. Qed.
  Instance closed_decision env e : Decision (Closed env e).
  Proof. rewrite /Closed. apply _. Qed.
  Instance: Proper ((≡) ==> (=) ==> (=)) is_closed.
  Proof.
    intros X Y HXY e e' Hee'. 
    fold_leibniz. by subst.
  Defined.  

  (** ** Values *)
  Inductive val :=
  | RecV (f x : binder) (e : expr) `{!Closed (x :b: f :b: ∅) e}
  | TLamV (e : expr) `{!Closed ∅ e} (* only closed lambdas are values *)
  | LitV (l : literal)
  | PairV (v1 v2 : val)
  | InjLV (v : val)
  | InjRV (v : val)
  | FoldV (v : val)
  | PackV (v : val).
  Bind Scope val_scope with val.

  (* Notation for bool and nat *)
  Fixpoint binop_eval (op : binop) (v1 v2 : val) : option val :=
    match op,v1,v2 with
    | Add, LitV (Nat a), LitV (Nat b) => Some $ LitV (Nat (a + b))
    | Sub, LitV (Nat a), LitV (Nat b) => Some $ LitV (Nat (a - b))
    | Eq, LitV (Nat a), LitV (Nat b) => Some $ if eq_nat_dec a b then LitV (Bool true) else LitV (Bool false)
    | Eq, LitV (Bool a), LitV (Bool b) => Some $ LitV (Bool (eqb a b))
    | Le, LitV (Nat a), LitV (Nat b) => Some $ if le_dec a b then LitV (Bool true) else LitV (Bool false)
    | Lt, LitV (Nat a), LitV (Nat b) => Some $ if lt_dec a b then LitV (Bool true) else LitV (Bool false)
    | Xor, LitV (Bool a), LitV (Bool b) => Some $ LitV (Bool (xorb a b))
    | _,_,_ => None
    end.
  Instance val_inh : Inhabited val := populate (LitV Unit).

  Fixpoint of_val (v : val) : expr :=
    match v with
    | RecV f x e _ => Rec f x e
    | TLamV e _ => TLam e
    | LitV l => Lit l
    | PairV v1 v2 => Pair (of_val v1) (of_val v2)
    | InjLV v => InjL (of_val v)
    | InjRV v => InjR (of_val v)
    | FoldV v => Fold (of_val v)
    | PackV v => Pack (of_val v)
    end.

  Fixpoint to_val (e : expr) : option val :=
    match e with
    | Rec f x e =>
      if decide (Closed (x :b: f :b: ∅) e)
      then Some (RecV f x e)
      else None
    | TLam e => 
      if decide (Closed ∅ e)
      then Some (TLamV e)
      else None
    | Lit l => Some (LitV l)
    | Pair e1 e2 => v1 ← to_val e1; v2 ← to_val e2; Some (PairV v1 v2)
    | InjL e => InjLV <$> to_val e
    | InjR e => InjRV <$> to_val e
    | Fold e => v ← to_val e; Some (FoldV v)
    | Pack e => v ← to_val e; Some (PackV v)
    | _ => None
    end.

  Lemma to_of_val v : to_val (of_val v) = Some v.
  Proof.
      by induction v; simplify_option_eq; repeat f_equal; try apply (proof_irrel _).
  Qed.

  Lemma of_to_val e v : to_val e = Some v → of_val v = e.
  Proof.
    revert v; induction e; intros v ?; simplify_option_eq; auto with f_equal.
  Qed.

  Instance of_val_inj : Inj (=) (=) of_val.
  Proof. by intros ?? Hv; apply (inj Some); rewrite -!to_of_val Hv. Qed.

  Instance val_dec_eq : EqDecision val.
  Proof.
    refine (fun v v' => cast_if (decide (of_val v = of_val v'))); abstract naive_solver.
  Defined.

  Instance literal_countable : Countable literal.
  Proof.
    refine (inj_countable' (λ l, match l with
    | Nat n => inl (inl n) | Bool b => inl (inr b)
    | Unit => inr (inl ()) | Loc l => inr (inr l)
    end) (λ l, match l with
    | inl (inl n) => Nat n | inl (inr b) => Bool b
    | inr (inl ()) => Unit | inr (inr l) => Loc l
    end) _); by intros [].
  Qed.

  Instance binop_countable : Countable binop.
  Proof.
   refine (inj_countable' (λ op, match op with
    | Add => 0 | Sub => 1 | Eq => 2 | Le => 3 | Lt => 4 | Xor => 5
    end) (λ n, match n with
    | 0 => Add | 1 => Sub | 2 => Eq | 3 => Le | 4 => Lt | _ => Xor
    end) _); by intros [].
  Qed.

  Instance expr_countable : Countable expr.
  Proof.
    set (enc := fix go e :=
           match e with
           | Var x => GenLeaf (inl (inl x))
           | Rec f x e => GenNode 0 [GenLeaf (inl (inr f)); GenLeaf (inl (inr x)); go e]
           | App e1 e2 => GenNode 1 [go e1; go e2]
           | Lit l => GenLeaf (inr (inl l))
           | Fold e => GenNode 2 [go e]
           | BinOp op e1 e2 => GenNode 3 [GenLeaf (inr (inr op)); go e1; go e2]
           | If e0 e1 e2 => GenNode 4 [go e0; go e1; go e2]
           | Pair e1 e2 => GenNode 5 [go e1; go e2]
           | Fst e => GenNode 6 [go e]
           | Snd e => GenNode 7 [go e]
           | InjL e => GenNode 8 [go e]
           | InjR e => GenNode 9 [go e]
           | Case e0 e1 e2 => GenNode 10 [go e0; go e1; go e2]
           | Fork e => GenNode 11 [go e]
           | Alloc e => GenNode 12 [go e]
           | Load e => GenNode 13 [go e]
           | Store e1 e2 => GenNode 14 [go e1; go e2]
           | CAS e0 e1 e2 => GenNode 15 [go e0; go e1; go e2]
           | Unfold e => GenNode 16 [go e]
           | TLam e => GenNode 17 [go e]
           | TApp e => GenNode 18 [go e]
           | Pack e => GenNode 19 [go e]
           | Unpack e1 e2 => GenNode 20 [go e1; go e2]
           end).
    set (dec := fix go e :=
           match e with
           | GenLeaf (inl (inl x)) => Var x
           | GenNode 0 [GenLeaf (inl (inr f)); GenLeaf (inl (inr x)); e] => Rec f x (go e)
           | GenNode 1 [e1; e2] => App (go e1) (go e2)
           | GenLeaf (inr (inl l)) => Lit l
           | GenNode 2 [e] => Fold (go e)
           | GenNode 3 [GenLeaf (inr (inr op)); e1; e2] => BinOp op (go e1) (go e2)
           | GenNode 4 [e0; e1; e2] => If (go e0) (go e1) (go e2)
           | GenNode 5 [e1; e2] => Pair (go e1) (go e2)
           | GenNode 6 [e] => Fst (go e)
           | GenNode 7 [e] => Snd (go e)
           | GenNode 8 [e] => InjL (go e)
           | GenNode 9 [e] => InjR (go e)
           | GenNode 10 [e0; e1; e2] => Case (go e0) (go e1) (go e2)
           | GenNode 11 [e] => Fork (go e)
           | GenNode 12 [e] => Alloc (go e)
           | GenNode 13 [e] => Load (go e)
           | GenNode 14 [e1; e2] => Store (go e1) (go e2)
           | GenNode 15 [e0; e1; e2] => CAS (go e0) (go e1) (go e2)
           | GenNode 16 [e] => Unfold (go e)
           | GenNode 17 [e] => TLam (go e)
           | GenNode 18 [e] => TApp (go e)
           | GenNode 19 [e] => Pack (go e)
           | GenNode 20 [e1; e2] => Unpack (go e1) (go e2)
           | _ => Lit Unit (* dummy *)
           end).
    refine (inj_countable' enc dec _). intros e. induction e; f_equal/=; auto.
  Qed.

  Instance val_countable : Countable val.
  Proof. refine (inj_countable of_val to_val _); auto using to_of_val. Qed.

  (** ** Evaluation contexts *)
  Inductive ectx_item :=
  | AppLCtx (e2 : expr)
  | AppRCtx (v1 : val)
  | TAppCtx
  | PairLCtx (e2 : expr)
  | PairRCtx (v1 : val)
  | BinOpLCtx (op : binop) (e2 : expr)
  | BinOpRCtx (op : binop) (v1 : val)
  | FstCtx
  | SndCtx
  | InjLCtx
  | InjRCtx
  | CaseCtx (e1 : expr) (e2 : expr)
  | IfCtx (e1 : expr) (e2 : expr)
  | FoldCtx
  | UnfoldCtx
  | PackCtx
  | UnpackLCtx (e2 : expr)
  | AllocCtx
  | LoadCtx
  | StoreLCtx (e2 : expr)
  | StoreRCtx (v1 : val)
  | CasLCtx (e1 : expr)  (e2 : expr)
  | CasMCtx (v0 : val) (e2 : expr)
  | CasRCtx (v0 : val) (v1 : val).

  Definition fill_item (Ki : ectx_item) (e : expr) : expr :=
    match Ki with
    | AppLCtx e2 => App e e2
    | AppRCtx v1 => App (of_val v1) e
    | TAppCtx => TApp e
    | PairLCtx e2 => Pair e e2
    | PairRCtx v1 => Pair (of_val v1) e
    | BinOpLCtx op e2 => BinOp op e e2
    | BinOpRCtx op v1 => BinOp op (of_val v1) e
    | FstCtx => Fst e
    | SndCtx => Snd e
    | InjLCtx => InjL e
    | InjRCtx => InjR e
    | CaseCtx e1 e2 => Case e e1 e2
    | IfCtx e1 e2 => If e e1 e2
    | FoldCtx => Fold e
    | UnfoldCtx => Unfold e
    | PackCtx => Pack e
    | UnpackLCtx e2 => Unpack e e2  
    | AllocCtx => Alloc e
    | LoadCtx => Load e
    | StoreLCtx e2 => Store e e2
    | StoreRCtx v1 => Store (of_val v1) e
    | CasLCtx e1 e2 => CAS e e1 e2
    | CasMCtx v0 e2 => CAS (of_val v0) e e2
    | CasRCtx v0 v1 => CAS (of_val v0) (of_val v1) e
    end.

  (** * Substitutions *)  
  (** Parallel substitutions and some properties are defined in [subst.v] *)
  Fixpoint subst (x : string) (es : expr) (e : expr)  : expr :=
    match e with
    | Var y => if decide (x = y) then es else Var y
    | Rec f y e =>
      Rec f y $ if decide (BNamed x ≠ f ∧ BNamed x ≠ y) then subst x es e else e
    | App e1 e2 => App (subst x es e1) (subst x es e2)
    | TLam e => TLam (subst x es e)
    | TApp e => TApp (subst x es e)
    | Lit l => Lit l
    | BinOp op e1 e2 => BinOp op (subst x es e1) (subst x es e2)
    | If e0 e1 e2 => If (subst x es e0) (subst x es e1) (subst x es e2)
    | Pair e1 e2 => Pair (subst x es e1) (subst x es e2)
    | Fst e => Fst (subst x es e)
    | Snd e => Snd (subst x es e)
    | Fold e => Fold (subst x es e)
    | Unfold e => Unfold (subst x es e)
    | Pack e => Pack (subst x es e)                
    | Unpack e0 e => 
      Unpack (subst x es e0) (subst x es e)
    | InjL e => InjL (subst x es e)
    | InjR e => InjR (subst x es e)
    | Case e0 e1 e2 => 
      Case (subst x es e0)
           (subst x es e1)
           (subst x es e2)
    | Fork e => Fork (subst x es e)
    | Alloc e => Alloc (subst x es e)
    | Load e => Load (subst x es e)
    | Store e1 e2 => Store (subst x es e1) (subst x es e2)
    | CAS e0 e1 e2 => CAS (subst x es e0) (subst x es e1) (subst x es e2)
  end.

  Definition subst' (mx : binder) (es : expr) : expr → expr :=
    match mx with BNamed x => subst x es | BAnon => id end.
 
  (** * Reduction semantics *)
  Definition state : Type := gmap loc val.

  Inductive head_step : expr → state → expr → state → list expr → Prop :=
  (* β *)
  | BetaS f x e1 e2 v2 σ e' :
      to_val e2 = Some v2 →
      Closed (x :b: f :b: ∅) e1 →
      (* e' = subst_p (<[x:=e2]>{[f:=(Rec f x e1)]}) e1 → *)
      e' = subst' f (Rec f x e1) (subst' x e2 e1) →
      head_step (App (Rec f x e1) e2) σ e' σ []
  (* Products *)
  | FstS e1 v1 e2 v2 σ :
      to_val e1 = Some v1 → to_val e2 = Some v2 →
      head_step (Fst (Pair e1 e2)) σ e1 σ []
  | SndS e1 v1 e2 v2 σ :
      to_val e1 = Some v1 → to_val e2 = Some v2 →
      head_step (Snd (Pair e1 e2)) σ e2 σ []
  (* Sums *)
  | CaseLS e0 v0 e1 e2 σ e' :
      to_val e0 = Some v0 →
      e' = App e1 e0 →
      head_step (Case (InjL e0) e1 e2) σ e' σ []
  | CaseRS e0 v0 e1 e2 σ e' :
      to_val e0 = Some v0 →
      e' = App e2 e0 →
      head_step (Case (InjR e0) e1 e2) σ e' σ []
  (* nat bin op *)
  | BinOpS op e1 e2 v1 v2 v σ :
      to_val e1 = Some v1 →
      to_val e2 = Some v2 →
      binop_eval op v1 v2 = Some v →
      head_step (BinOp op e1 e2) σ (of_val v) σ []
  (* If then else *)
  | IfFalse e1 e2 σ :
      head_step (If (Lit (Bool false)) e1 e2) σ e2 σ []
  | IfTrue e1 e2 σ :
      head_step (If (Lit (Bool true)) e1 e2) σ e1 σ []
  (* Recursive Types *)
  | Unfold_Fold e v σ :
      to_val e = Some v →
      head_step (Unfold (Fold e)) σ e σ []
  (* Polymorphic Types *)
  | TBeta e σ :
      Closed ∅ e →
      head_step (TApp (TLam e)) σ e σ []
  (* Existential types *)
  | Unpack_Pack e1 v e2 σ e' :
      to_val e1 = Some v →
      e' = App e2 e1 →
      head_step (Unpack (Pack e1) e2) σ e' σ []
  (* Concurrency *)
  | ForkS e σ:
      head_step (Fork e) σ (Lit Unit) σ [e]
  (* Reference Types *)
  | AllocS e v σ l :
     to_val e = Some v → σ !! l = None →
     head_step (Alloc e) σ (Lit (Loc l)) (<[l:=v]>σ) []
  | LoadS l v σ :
     σ !! l = Some v →
     head_step (Load (Lit (Loc l))) σ (of_val v) σ []
  | StoreS l e v σ :
     to_val e = Some v → is_Some (σ !! l) →
     head_step (Store (Lit (Loc l)) e) σ (Lit Unit) (<[l:=v]>σ) []
  (* Compare and swap *)
  | CasFailS l e1 v1 e2 v2 vl σ :
     to_val e1 = Some v1 → to_val e2 = Some v2 →
     σ !! l = Some vl → vl ≠ v1 →
     head_step (CAS (Lit (Loc l)) e1 e2) σ (Lit (Bool false)) σ []
  | CasSucS l e1 v1 e2 v2 σ :
     to_val e1 = Some v1 → to_val e2 = Some v2 →
     σ !! l = Some v1 →
     head_step (CAS (Lit (Loc l)) e1 e2) σ (Lit (Bool true)) (<[l:=v2]>σ) [].

  Definition is_atomic (e : expr) : Prop :=
    match e with
    | Alloc e => is_Some (to_val e)
    | Load e =>  is_Some (to_val e)
    | Store e1 e2 => is_Some (to_val e1) ∧ is_Some (to_val e2)
    | CAS e0 e1 e2 => is_Some (to_val e0) ∧ is_Some (to_val e1) ∧ is_Some (to_val e2)
    | Fork _ => True
    | _ => False
    end.

  Canonical Structure stateC := leibnizC state.
  Canonical Structure valC := leibnizC val.
  Canonical Structure exprC := leibnizC expr.
  Canonical Structure locC := leibnizC loc.
  Canonical Structure ectx_itemC := leibnizC ectx_item.

  Lemma fill_item_val Ki e :
    is_Some (to_val (fill_item Ki e)) → is_Some (to_val e).
  Proof. intros [v ?]. destruct Ki; simplify_option_eq; eauto. Qed.

  Instance fill_item_inj Ki : Inj (=) (=) (fill_item Ki).
  Proof. destruct Ki; intros ???; simplify_eq; auto with f_equal. Qed.

  Lemma val_stuck e1 σ1 e2 σ2 ef :
    head_step e1 σ1 e2 σ2 ef → to_val e1 = None.
  Proof. destruct 1; naive_solver. Qed.

  Lemma head_ctx_step_val Ki e σ1 e2 σ2 ef :
    head_step (fill_item Ki e) σ1 e2 σ2 ef → is_Some (to_val e).
  Proof. destruct Ki; inversion_clear 1; simplify_option_eq; by eauto. Qed.

  Lemma fill_item_no_val_inj Ki1 Ki2 e1 e2 :
    to_val e1 = None → to_val e2 = None →
    fill_item Ki1 e1 = fill_item Ki2 e2 → Ki1 = Ki2.
  Proof.
    destruct Ki1, Ki2; intros; try discriminate; simplify_eq;
    repeat match goal with
           | H : to_val (of_val _) = None |- _ => by rewrite to_of_val in H
           end; auto.
  Qed.

  Lemma alloc_fresh e v σ :
    let l := fresh (dom (gset loc) σ) in
    to_val e = Some v → head_step (Alloc e) σ (Lit (Loc l)) (<[l:=v]>σ) [].
  Proof. by intros; apply AllocS, (not_elem_of_dom (D:=gset loc)), is_fresh. Qed.
End lang.

(** Language instance for Iris *)
Program Instance heap_ectxi_lang :
  EctxiLanguage
    (lang.expr) lang.val lang.ectx_item lang.state := {|
  of_val := lang.of_val; to_val := lang.to_val;
  fill_item := lang.fill_item; head_step := lang.head_step
|}.
Solve Obligations with eauto using lang.to_of_val, lang.of_to_val,
  lang.val_stuck, lang.fill_item_val, lang.fill_item_no_val_inj,
  lang.head_ctx_step_val.

Canonical Structure lang := ectx_lang (lang.expr).

Export lang.

(* This lemma is helpful for tactics on locked values and for reifying locked values. *)
Lemma of_val_unlock v e : of_val v = e → of_val (locked v) = e.
Proof. by unlock. Qed.

Local Hint Resolve language.val_irreducible.
Local Hint Resolve to_of_val.
Local Hint Unfold language.irreducible.

Lemma is_atomic_correct e : is_atomic e → language.atomic e.
Proof.
  intros ?; apply ectx_language_atomic.
  - destruct 1; simpl; eauto.
  - apply ectxi_language_sub_values=> /= Ki e' Hfill.
    destruct e=> //; destruct Ki; repeat (simplify_eq/=; case_match=>//);
                   naive_solver eauto using to_val_is_Some.
Qed.

Ltac solve_atomic :=
  apply is_atomic_correct; simpl; repeat split;
    rewrite ?to_of_val; eapply mk_is_Some; fast_done.

Hint Extern 0 (language.atomic _) => solve_atomic.
Hint Extern 0 (language.atomic _) => solve_atomic : typeclass_instances.

(* Some other useful tactics *)
Ltac inv_head_step :=
  repeat match goal with
  | _ => progress simplify_map_eq/= (* simplify memory stuff *)
  | H : to_val _ = Some _ |- _ => apply of_to_val in H
  | H : _ = of_val ?v |- _ =>
     is_var v; destruct v; first[discriminate H|injection H as H]
  | H : head_step ?e _ _ _ _ |- _ =>
     try (is_var e; fail 1); (* inversion yields many goals if [e] is a variable
     and can thus better be avoided. *)
     inversion H; subst; clear H
  end.
