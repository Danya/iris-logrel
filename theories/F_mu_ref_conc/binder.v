(* This module defines binders and liftings of all the necessary
operations/lemmas. *)
From iris.algebra Require Export base.
From iris_logrel.prelude Require Export base.

Inductive binder := BAnon | BNamed : string → binder.
Delimit Scope binder_scope with bind.
Bind Scope binder_scope with binder.

Definition cons_binder (mx : binder) (X : stringset) : stringset :=
  match mx with BAnon => X | BNamed x => {[x]} ∪ X end.
Infix ":b:" := cons_binder (at level 60, right associativity).
Instance binder_eq_dec_eq : EqDecision binder.
Proof. solve_decision. Defined.
Instance binder_countable : Countable binder.
Proof.
 refine (inj_countable' (λ b, match b with BNamed s => Some s | BAnon => None end)
  (λ b, match b with Some s => BNamed s | None => BAnon end) _); by intros [].
Qed.

Instance set_unfold_cons_binder x mx X P :
  SetUnfold (x ∈ X) P → SetUnfold (x ∈ mx :b: X) (BNamed x = mx ∨ P).
Proof.
  constructor. rewrite -(set_unfold (x ∈ X) P).
  destruct mx; rewrite /= ?elem_of_union ?elem_of_singleton; naive_solver.
Qed.

(** Operation lifts *)
Instance delete_binder (A : Type) : Delete binder (stringmap A) :=
  λ b, match b with
       | BAnon => id
       | BNamed s => delete s
       end.
Instance insert_binder (A : Type): Insert binder A (stringmap A) :=
  fun k τ Γ =>
    match k with
    | BAnon => Γ
    | BNamed x => <[x:=τ]>Γ
    end.
Instance singleton_binder (A : Type) : SingletonM binder A (stringmap A) :=
  λ k v, match k with
         | BAnon => ∅
         | BNamed x => {[x:=v]}
         end.
Instance singleton_binder_set : Singleton binder stringset :=
  λ x, match x with
       | BAnon => ∅
       | BNamed s => {[s]}
       end.

(** Properties lifts *)
Lemma dom_delete_binder {A} (i : binder) (m : stringmap A) :
  dom stringset (delete i m) ≡ (dom stringset m) ∖ {[i]}.
Proof.
  destruct i; cbn-[difference]; eauto.
  - set_solver.
  - apply dom_delete.
Qed.

(* TODO: We need to put this instance *after* dom_delete_binder, otherwise the proof of dom_delete_bindert does not work *)
Definition lookup_binder {A} : Lookup binder A (stringmap A) :=
  λ k m, match k with
         | BAnon => None
         | BNamed s => m !! s
         end.

Lemma dom_insert_binder {A} (m : stringmap A) (i : binder) (x : A) :
  dom stringset (<[i:=x]> m) = {[i]} ∪ dom stringset m.
Proof. destruct i; cbn-[union]; unfold_leibniz. set_solver.
       apply dom_insert. Qed.
Lemma cons_binder_union (i : binder) (X : stringset) :
  i :b: X = {[i]} ∪ X.
Proof. destruct i; cbn-[union]; eauto. set_solver. Qed.

Lemma lookup_insert_binder {A} (i : binder) (j : string) (x : A) (m : stringmap A):
  i = BNamed j → <[i:=x]>m !! j = Some x.
Proof. intros ->. apply lookup_insert. Qed.
Lemma lookup_insert_ne_binder {A} (i : binder) (j : string) (x : A) (m : stringmap A):
  i ≠ BNamed j → <[i:=x]>m !! j = m !! j.
Proof. intros Hij. destruct i; cbn; auto. apply lookup_insert_ne.
       intro Hsj. destruct Hij. by f_equal. Qed.
Lemma insert_union_r_binder {A : Type} (m1 m2 : stringmap A) (i : binder) (x : A) :
  lookup_binder i m1 = None → <[i:=x]>(m1 ∪ m2) = m1 ∪ <[i:=x]>m2.
Proof. destruct i; auto. intros. by apply insert_union_r. Qed.
Lemma insert_union_l_binder {A : Type} (m1 m2 : stringmap A) (i : binder) (x : A) :
  <[i:=x]>(m1 ∪ m2) = <[i:=x]> m1 ∪ m2.
Proof. destruct i; auto. by apply insert_union_l. Qed.
Lemma lookup_union {A : Type} (m1 m2 : stringmap A) (i : string) :
  (m1 ∪ m2) !! i = (match m1 !! i with Some s => Some s | None => m2 !! i end).
Proof. remember (m1 !! i) as m1i. destruct m1i.
       + apply lookup_union_Some_l; auto.
       + remember (m2 !! i) as m2i. destruct m2i; auto.
         apply lookup_union_Some_raw; auto.
         apply lookup_union_None; auto.
Qed.
Lemma insert_union_insert_l_binder {A : Type} (m1 m2 : stringmap A) (i : binder) (x y : A) :
  <[i:=x]>(m1 ∪ <[i:=y]>m2) = <[i:=x]> (m1 ∪ m2).
Proof. destruct i; cbn; auto. apply map_eq. intro i.
       destruct (decide (i = s)); subst.
       + by rewrite ?lookup_insert.
       + rewrite ?lookup_insert_ne; auto.
      rewrite ?lookup_union. rewrite lookup_insert_ne; auto.
Qed.
Lemma insert_insert_binder {A} (m : stringmap A) (i : binder) (x y : A) :
  <[i:=x]> (<[i:=y]> m) = <[i:=x]> m.
Proof. destruct i; auto. by apply insert_insert. Qed.

Lemma insert_delete_binder {A} (m : stringmap A) (i : binder) (x : A) :
  <[i:=x]> (delete i m) = <[i:=x]> m.
Proof. destruct i; auto. by apply insert_delete. Qed.

Lemma delete_insert_binder {A} (m : stringmap A) (i : binder) (x : A) :
  lookup_binder i m = None → delete i (<[i:=x]> m) = m.
Proof. destruct i; cbn; eauto. apply delete_insert. Qed.
Lemma delete_insert_ne_binder {A} (m : stringmap A) (i j : binder) (x : A) :
  i ≠ j → delete i (<[j:=x]> m) = <[j:=x]> (delete i m).
Proof. destruct i, j; auto. intros Hs. apply delete_insert_ne.
       intro Hs0. destruct Hs. by f_equal.
Qed.
Lemma delete_insert_ne_binder' {A} (m : stringmap A) (i : binder) (j : string) (x : A) :
  i ≠ BNamed j → delete i (<[j:=x]> m) = <[j:=x]> (delete i m).
Proof.
  destruct i; cbn; eauto. 
  intro Hs.
  apply delete_insert_ne. intro Hs'. apply Hs. by subst.
Qed.

Lemma lookup_delete_binder {A} (m : stringmap A) (i : binder) :
  lookup_binder i (delete i m) = None.
Proof. destruct i; auto. cbn. apply lookup_delete. Qed.
Lemma delete_commute_binder {A} (m : stringmap A) (i j : binder) :
  delete i (delete j m) = delete j (delete i m).
Proof. destruct i, j; cbn; auto. apply delete_commute. Qed.

Lemma delete_empty_binder {A} (x : binder) :
  delete x (∅ : stringmap A) = ∅.
Proof. destruct x; cbn; eauto. apply delete_empty. Qed.

Lemma fmap_insert' {A B} (x : binder) (f : A → B) v (vs : stringmap A) :
  f <$> <[x:=v]>vs = <[x:=f v]> (f <$> vs).
Proof.
  destruct x; cbn; auto.
  by rewrite fmap_insert.
Qed.

Lemma delete_idemp_binder {A} (x : binder) (m : stringmap A) :
  delete x (delete x m) = delete x m.
Proof.
  destruct x; cbn; eauto. apply delete_idemp.
Qed.
