From iris.proofmode Require Import tactics.
From iris_logrel.F_mu_ref_conc Require Export typing context_refinement.
From iris_logrel.logrel Require Export rules_threadpool.
Import uPred.

(* HACK: move somewhere else *)
Ltac auto_equiv :=
  (* Deal with "pointwise_relation" *)
  repeat lazymatch goal with
  | |- pointwise_relation _ _ _ _ => intros ?
  end;
  (* Normalize away equalities. *)
  repeat match goal with
  | H : _ ≡{_}≡ _ |-  _ => apply (timeless_iff _ _) in H
  | _ => progress simplify_eq
  end;
  (* repeatedly apply congruence lemmas and use the equalities in the hypotheses. *)
  try (f_equiv; fast_done || auto_equiv).

(* TODO: modify this in coq-stdpp or something *)
(* this is /almost/ [solve_proper_core ltac:(fun _ => auto_equiv)] *)
(* but we do an additional simpl auto [simplify_eq] *)
Ltac solve_proper ::=
  intros;
  repeat lazymatch goal with
  | |- Proper _ _ => intros ???
  | |- (_ ==> _)%signature _ _ => intros ???
  | |- pointwise_relation _ _ _ _ => intros ?
  | |- ?R ?f _ => try let f' := constr:(λ x, f x) in intros ?
  end; simplify_eq/=;
  (solve_proper_unfold + idtac);
  solve [repeat first [eassumption | auto_equiv] ].

Definition logN : namespace := logrelN .@ "logN".

Section semtypes.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types τi : D.
  Implicit Types Δ : listC D.
  Implicit Types interp : listC D → D.

  Definition interp_expr (E1 E2 : coPset) (τi : listC D -n> D) (Δ : listC D)
      (ee : expr * expr) : iProp Σ := (∀ j K,
    j ⤇ fill K (ee.2) -∗
    |={E1,E2}=> WP ee.1 {{ v, ∃ v', j ⤇ fill K (of_val v') ∗ τi Δ (v, v') }})%I.
  Global Instance interp_expr_ne n E1 E2:
    Proper (dist n ==> dist n ==> (=) ==> dist n) (interp_expr E1 E2).
  Proof. solve_proper. Qed.       

  Program Definition ctx_lookup (x : var) : listC D -n> D := λne Δ ww,
    (□ from_option id (cconst True)%I (Δ !! x) ww)%I.
  Solve Obligations with solve_proper.

  Program Definition interp_unit : listC D -n> D := λne Δ ww,
    (⌜ww.1 = #()⌝ ∧ ⌜ww.2 = #()⌝)%I.
  Solve Obligations with solve_proper_alt.

  Program Definition interp_nat : listC D -n> D := λne Δ ww,
    (∃ n : nat, ⌜ww.1 = #n⌝ ∧ ⌜ww.2 = #n⌝)%I.
  Solve Obligations with solve_proper.

  Program Definition interp_bool : listC D -n> D := λne Δ ww,
    (∃ b : bool, ⌜ww.1 = #b⌝ ∧ ⌜ww.2 = #b⌝)%I.
  Solve Obligations with solve_proper.

  Program Definition interp_prod
      (interp1 interp2 : listC D -n> D) : listC D -n> D := λne Δ ww,
    (∃ vv1 vv2, ⌜ww = (PairV (vv1.1) (vv2.1), PairV (vv1.2) (vv2.2))⌝ ∧
                interp1 Δ vv1 ∧ interp2 Δ vv2)%I.
  Solve Obligations with solve_proper. 

  Program Definition interp_sum
      (interp1 interp2 : listC D -n> D) : listC D -n> D := λne Δ ww,
    ((∃ vv, ⌜ww = (InjLV (vv.1), InjLV (vv.2))⌝ ∧ interp1 Δ vv) ∨
     (∃ vv, ⌜ww = (InjRV (vv.1), InjRV (vv.2))⌝ ∧ interp2 Δ vv))%I.
  Solve Obligations with solve_proper. 

  Program Definition interp_arrow (E1 E2 : coPset)
          (interp1 interp2 : listC D -n> D) : listC D -n> D :=
    λne Δ ww,
    (□ ∀ vv, interp1 Δ vv →
             interp_expr E1 E2 
               interp2 Δ (App (of_val (ww.1)) (of_val (vv.1)),
                          App (of_val (ww.2)) (of_val (vv.2))))%I.
  Solve Obligations with solve_proper. 

  Program Definition interp_forall (E1 E2 : coPset)
      (interp : listC D -n> D) : listC D -n> D := λne Δ ww,
    (□ ∀ τi,
          ⌜∀ ww, PersistentP (τi ww)⌝ →
          interp_expr E1 E2
            interp (τi :: Δ) (TApp (of_val (ww.1)), TApp (of_val (ww.2))))%I.
  Solve Obligations with solve_proper.

  Program Definition interp_exists
      (interp : listC D -n> D) : listC D -n> D := λne Δ ww,
    (∃ vv, ⌜ww = (PackV (vv.1), PackV (vv.2))⌝ 
     ∧ ∃ τi : D, ⌜∀ ww, PersistentP (τi ww)⌝ ∧ interp (τi :: Δ) vv)%I.
  Solve Obligations with solve_proper.

  Program Definition interp_rec1
      (interp : listC D -n> D) (Δ : listC D) (τi : D) : D := λne ww,
    (∃ vv, ⌜ww = (FoldV (vv.1), FoldV (vv.2))⌝ ∧ ▷ interp (τi :: Δ) vv)%I.
  Solve Obligations with solve_proper.

  Global Instance interp_rec1_contractive
    (interp : listC D -n> D) (Δ : listC D) : Contractive (interp_rec1 interp Δ).
  Proof. solve_contractive. Qed.

  Program Definition interp_rec (interp : listC D -n> D) : listC D -n> D := λne Δ,
    fixpoint (interp_rec1 interp Δ).
  Next Obligation.
    intros interp n Δ1 Δ2 HΔ; apply fixpoint_ne => τi ww. solve_proper.
  Qed.

  Program Definition interp_ref_inv (ll : loc * loc) : D -n> iProp Σ := λne τi,
    (∃ vv, ll.1 ↦ᵢ vv.1 ∗ ll.2 ↦ₛ vv.2 ∗ τi vv)%I.
  Solve Obligations with solve_proper.

  Program Definition interp_ref
      (interp : listC D -n> D) : listC D -n> D := λne Δ ww,
    (∃ ll, ⌜ww = (LitV (Loc (ll.1)), LitV (Loc (ll.2)))⌝ ∧
           inv (logN .@ ll) (interp_ref_inv ll (interp Δ)))%I.
  Solve Obligations with solve_proper.

  Program Definition interp_singleton (v v' : val) : listC D -n> D :=
    λne Δ ww, ⌜ww = (v,v')⌝%I.
  Solve Obligations with solve_proper.       

  Program Definition interp_iref
      (interp : listC D -n> D) : listC D -n> D := λne Δ ww,
    (∃ (ll : loc * loc), 
       interp_ref (interp_singleton (LitV (Loc (ll.1))) (LitV (Loc (ll.2)))) Δ ww ∧
       inv (logN .@ ll) (interp_ref_inv ll (interp Δ)))%I.
  Solve Obligations with solve_proper.

  Fixpoint interp (E1 E2 : coPset) (τ : type) : listC D -n> D :=
    match τ return _ with
    | TUnit => interp_unit
    | TNat => interp_nat
    | TBool => interp_bool
    | TProd τ1 τ2 => interp_prod (interp E1 E2 τ1) (interp E1 E2 τ2)
    | TSum τ1 τ2 => interp_sum (interp E1 E2 τ1) (interp E1 E2 τ2)
    | TArrow τ1 τ2 =>
      interp_arrow ⊤ ⊤ (interp E1 E2 τ1) (interp ⊤ ⊤ τ2)
    | TVar x => ctx_lookup x
    | TForall τ' => interp_forall ⊤ ⊤ (interp E1 E2 τ')
    | TExists τ' => interp_exists (interp E1 E2 τ')
    | TRec τ' => interp_rec (interp E1 E2 τ')
    | Tref τ' => interp_ref (interp E1 E2 τ')
    end.
  Notation "⟦ τ ⟧" := (interp ⊤ ⊤ τ).
  Notation "⟦ τ ⟧ₑ" := (interp_expr ⊤ ⊤ ⟦ τ ⟧).

  Global Instance interp_persistent τ E1 E2 Δ vv :
    PersistentP (interp E1 E2 τ Δ vv).
  Proof.
    revert vv Δ; induction τ=> vv Δ; simpl; try apply _.
    rewrite /PersistentP /interp_rec fixpoint_unfold /interp_rec1 /=. eauto.
  Qed.

  (* DF: automate this proof some more *)
  Lemma interp_weaken Δ1 Π Δ2 E1 E2 τ :
    interp E1 E2 (τ.[upn (length Δ1) (ren (+ length Π))]) (Δ1 ++ Π ++ Δ2)
    ≡ interp E1 E2 τ (Δ1 ++ Δ2).
  Proof.
    revert Δ1 Π Δ2 E1 E2. induction τ=> Δ1 Π Δ2 E1 E2; simpl; auto.
    - intros ww; simpl; properness; auto. by apply IHτ1. by apply IHτ2.
    - intros ww; simpl; properness; auto. by apply IHτ1. by apply IHτ2.
    - unfold interp_expr.
      intros ww; simpl; properness; auto. by apply IHτ1. by apply IHτ2.
    - apply fixpoint_proper=> τi ww /=.
      properness; auto. apply (IHτ (_ :: _)).
    - intros ww; simpl; properness; auto.
      rewrite iter_up; destruct lt_dec as [Hl | Hl]; simpl.
      { by rewrite !lookup_app_l. }
      rewrite !lookup_app_r; [|lia ..]. do 4 f_equiv. lia. 
    - unfold interp_expr.
      intros ww; simpl; properness; auto. by apply (IHτ (_ :: _)).
    - intros ww; simpl; properness; auto. by apply (IHτ (_ :: _)).
    - intros ww; simpl; properness; auto. by apply IHτ.
  Qed.

  Lemma interp_subst_up Δ1 Δ2 τ τ' :
    interp ⊤ ⊤ τ (Δ1 ++ interp ⊤ ⊤ τ' Δ2 :: Δ2)
    ≡ interp ⊤ ⊤ (τ.[upn (length Δ1) (τ' .: ids)]) (Δ1 ++ Δ2).
  Proof.
    revert Δ1 Δ2; induction τ=> Δ1 Δ2; simpl; auto.
    - intros ww; simpl; properness; auto. by apply IHτ1. by apply IHτ2.
    - intros ww; simpl; properness; auto. by apply IHτ1. by apply IHτ2.
    - unfold interp_expr.
      intros ww; simpl; properness; auto. by apply IHτ1. by apply IHτ2.
    - apply fixpoint_proper=> τi ww /=.
      properness; auto. apply (IHτ (_ :: _)).
    - intros ww; simpl.
      rewrite iter_up; destruct lt_dec as [Hl | Hl]; simpl.
      { by rewrite !lookup_app_l. }
      rewrite !lookup_app_r; [|lia ..].
      destruct (x - length Δ1) as [|n] eqn:?; simpl.
      { symmetry. rewrite always_always. asimpl. apply (interp_weaken [] Δ1 Δ2 _ _ τ'). }
      rewrite !lookup_app_r; [|lia ..]. do 4 f_equiv. lia. 
    - unfold interp_expr.
      intros ww; simpl; properness; auto. apply (IHτ (_ :: _)).
    - intros ww; simpl; properness; auto. apply (IHτ (_ :: _)).
    - intros ww; simpl; properness; auto. by apply IHτ.
  Qed.

  Lemma interp_subst Δ2 τ τ' :
    ⟦ τ ⟧ ((⟦ τ' ⟧ Δ2) :: Δ2) ≡ ⟦ τ.[τ'/] ⟧ Δ2.
  Proof. apply (interp_subst_up []). Qed.

  Lemma interp_expr_subst Δ2 τ τ' ww :
    ⟦ τ ⟧ₑ ((⟦ τ' ⟧ Δ2) :: Δ2) ww ≡ ⟦ τ.[τ'/] ⟧ₑ Δ2 ww.
  Proof.
    unfold interp_expr.
    properness; auto.
    apply interp_subst.
  Qed.

  Lemma interp_ren_up Δ1 Δ2 E τ τi :
    interp E E τ (Δ1 ++ Δ2) ≡ interp E E (τ.[upn (length Δ1) (ren (+1))]) (Δ1 ++ τi :: Δ2).
  Proof.
    revert E Δ1 Δ2. induction τ => E Δ1 Δ2; simpl; eauto.
    - intros vv; simpl; properness; eauto. by apply IHτ1. by apply IHτ2.
    - intros vv; simpl; properness; eauto. by apply IHτ1. by apply IHτ2.
    - unfold interp_expr. intros vv; simpl; properness; eauto. by apply IHτ1. by apply IHτ2.
    - apply fixpoint_proper=> τ' ww /=.
      properness; auto. apply (IHτ _ (_ :: _)).
    - intros vv; simpl.
      rewrite iter_up; destruct lt_dec as [Hl | Hl]; simpl; properness.
      { by rewrite !lookup_app_l. }
      rewrite !lookup_app_r; [|lia ..].
      assert ((length Δ1 + S (x - length Δ1) - length Δ1) = S (x - length Δ1)) as Hwat.
      { lia. }
      rewrite Hwat. simpl. done.
    - unfold interp_expr.
      intros vv; simpl; properness; auto. apply (IHτ _ (_ :: _)).
    - intros vv; simpl; properness; auto. apply (IHτ _ (_ :: _)).
    - intros vv; simpl; properness; auto. by apply IHτ.
  Qed.

  Lemma interp_ren τ τi E Δ :
    interp E E τ Δ ≡ interp E E (τ.[ren (+1)]) (τi :: Δ).
  Proof. by apply (interp_ren_up []). Qed.

  Lemma interp_EqType_agree τ v v' E1 E2 Δ :
    EqType τ → interp E1 E2 τ Δ (v, v') ⊢ ⌜v = v'⌝.
  Proof.
    intros Hτ; revert v v'; induction Hτ; iIntros (v v') "#H1 /=".
    - by iDestruct "H1" as "[% %]"; subst.
    - by iDestruct "H1" as (n) "[% %]"; subst.
    - by iDestruct "H1" as (b) "[% %]"; subst.
    - iDestruct "H1" as ([??] [??]) "[% [H1 H2]]"; simplify_eq/=.
      rewrite IHHτ1 IHHτ2.
      by iDestruct "H1" as "%"; iDestruct "H2" as "%"; subst.
    - iDestruct "H1" as "[H1|H1]".
      + iDestruct "H1" as ([??]) "[% H1]"; simplify_eq/=.
        rewrite IHHτ1. by iDestruct "H1" as "%"; subst.
      + iDestruct "H1" as ([??]) "[% H1]"; simplify_eq/=.
        rewrite IHHτ2. by iDestruct "H1" as "%"; subst.
  Qed.

  (* TODO: derive this from [interp_EqType_agree] *)
  (* This formulation is more suitable for proving soundness of the logical relation in [soundness_binary.v] *)
  Lemma interp_ObsType_agree Δ τ : ∀ (v v' : val),
    ⟦ τ ⟧ Δ (v, v') ⊢ ⌜ObsType τ → v = v'⌝.
  Proof.
    induction τ; iIntros (v v') "HI"; simpl; eauto;
      try by (iPureIntro; inversion 1).
    - iDestruct "HI" as "[% %]"; subst; eauto.
    - iDestruct "HI" as (n) "[% %]"; subst; eauto.
    - iDestruct "HI" as (b) "[% %]"; subst; eauto.
    - iDestruct "HI" as ([v1 v1'] [v2 v2']) "/= [% [H1 H2]]".
      simplify_eq/=.
      iDestruct (IHτ1 with "H1") as %IH1.
      iDestruct (IHτ2 with "H2") as %IH2.
      iPureIntro. inversion 1; simplify_eq.
      rewrite IH1; auto.
      by rewrite IH2.
    - iDestruct "HI" as "[HI | HI]";
        iDestruct "HI" as ([w w']) "[% HI]"; simplify_eq/=.
      + iDestruct (IHτ1 with "HI") as %IH1. iPureIntro.
        inversion 1. by rewrite IH1.
      + iDestruct (IHτ2 with "HI") as %IH2. iPureIntro.
       inversion 1. by rewrite IH2.
  Qed.

  Lemma interp_ret E τ Δ e1 e2 v1 v2 :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    ⟦ τ ⟧ Δ (v1,v2) -∗ interp_expr E E ⟦ τ ⟧ Δ (e1,e2).
  Proof.
    iIntros (Hv1 Hv2) "Hτ".
    unfold interp_expr. iIntros (j K) "Hj /=".
    rewrite -(of_to_val e2 v2 Hv2).
    iModIntro.
    iApply wp_value; eauto.
  Qed.

End semtypes.

Notation "⟦ τ ⟧" := (interp ⊤ ⊤ τ).
Notation "⟦ τ ⟧ₑ" := (interp_expr ⊤ ⊤ ⟦ τ ⟧).
