From iris.proofmode Require Import tactics.
From iris.program_logic Require Import ectx_lifting.
From iris_logrel.logrel Require Export logrel_binary.
From iris_logrel.logrel Require Import rules_threadpool tactics_threadpool.
From iris_logrel.F_mu_ref_conc Require Import tactics pureexec.

(** * Properties of the relational interpretation *)
Section properties.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types e : expr.
  Implicit Types Δ : listC D.
  Hint Resolve to_of_val.

  (** * Lemmas to show that binary logical model is closed under
  (forward) reductions. *)
  
  Lemma bin_log_related_val Δ Γ E e e' τ v v' :
    to_val e = Some v →
    to_val e' = Some v' →
    (|={E}=> interp ⊤ ⊤ τ Δ (v, v')) ⊢ {E,E;Δ;Γ} ⊨ e ≤log≤ e' : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros (Hv Hv') "IH".
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj /=".
    replace e with (of_val v); auto using of_to_val.
    replace e' with (of_val v'); auto using of_to_val.
    rewrite /env_subst !Closed_subst_p_id.
    iMod "IH" as "IH".
    iModIntro. iApply wp_value; eauto.
  Qed.

  Lemma bin_log_related_arrow_val Δ Γ E (f x f' x' : binder) (e e' eb eb' : expr) (τ τ' : type) :
    e = (rec: f x := eb)%E →
    e' = (rec: f' x' := eb')%E →
    Closed ∅ e →
    Closed ∅ e' →
    □(∀ v1 v2, ⟦ τ ⟧ Δ (v1, v2) -∗
      {⊤,⊤;Δ;Γ} ⊨ App e (of_val v1) ≤log≤ App e' (of_val v2) : τ') -∗
    {E,E;Δ;Γ} ⊨ e ≤log≤ e' : TArrow τ τ'.
  Proof.
    iIntros (????) "#H".
    subst e e'.
    rewrite bin_log_related_eq.
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj".
    cbn-[subst_p].
    iModIntro.
    rewrite {2}/env_subst Closed_subst_p_id.
    iApply wp_value.
    { rewrite /IntoVal. simpl. erewrite decide_left. done. }
    rewrite /env_subst Closed_subst_p_id.
    iExists (RecV f' x' eb').
    iFrame "Hj". iAlways. iIntros ([v1 v2]) "Hvv".
    iSpecialize ("H" $! v1 v2 with "Hvv Hs []").
    { iAlways. iApply "HΓ". }    
    assert (Closed ∅ ((rec: f x := eb) v1)).
    { unfold Closed in *. simpl.
      intros. split_and?; auto. apply of_val_closed. }
    assert (Closed ∅ ((rec: f' x' := eb') v2)).
    { unfold Closed in *. simpl.
      intros. split_and?; auto. apply of_val_closed. }
    rewrite /env_subst. rewrite !Closed_subst_p_id. done.
  Qed.

  Lemma bin_log_related_arrow Δ Γ E (f x f' x' : binder) (f1 f2 eb eb' : expr) (τ τ' : type) :
    f1 = (rec: f x := eb)%E →
    f2 = (rec: f' x' := eb')%E →
    Closed ∅ f1 →
    Closed ∅ f2 →
    □(∀ (v1 v2 : val), {⊤,⊤;Δ;Γ} ⊨ v1 ≤log≤ v2 : τ -∗
      {⊤,⊤;Δ;Γ} ⊨ f1 v1 ≤log≤ f2 v2 : τ') -∗
    {E,E;Δ;Γ} ⊨ f1 ≤log≤ f2 : TArrow τ τ'.
  Proof.
    iIntros (????) "#H".
    iApply bin_log_related_arrow_val; eauto.
    iAlways. iIntros (v1 v2) "Hvv".
    iApply "H".
    iApply (related_ret ⊤).
    by iApply interp_ret.
  Qed.

  Lemma bin_log_related_spec_ctx Δ Γ E1 E2 e e' τ ℷ :
    (ℷ ⊢ (∃ ρ, spec_ctx ρ) -∗ {E1,E2;Δ;Γ} ⊨ e ≤log≤ e' : τ)%I →
    (ℷ ⊢ {E1,E2;Δ;Γ} ⊨ e ≤log≤ e' : τ)%I.
  Proof.
    intros Hp.
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros "Hctx". iIntros (vvs ρ') "#Hspec".
    rewrite (persistentP (spec_ctx _)).
    rewrite (uPred.always_sep_dup' (spec_ctx _)).
    iDestruct "Hspec" as "#[Hspec #Hspec']".
    iRevert "Hspec'".
    rewrite (uPred.always_elim (spec_ctx _)).
    iAssert (∃ ρ, spec_ctx ρ)%I as "Hρ".
    { eauto. }
    iClear "Hspec".
    iRevert (vvs ρ').
    fold (bin_log_related_def E1 E2 Δ Γ e e' τ).
    rewrite -bin_log_related_eq.
    iApply (Hp with "Hctx Hρ").
  Qed.

  (** ** Reductions on the left *)
  (** *** Lifting of the pure reductions *)
  Lemma bin_log_pure_l Δ (Γ : stringmap type) (E : coPset) 
    (K' : list ectx_item) (e e' t : expr) (τ : type)
    (Hsafe' : ∀ σ, reducible e σ)
    (Hdet' : ∀ σ1 e2 σ2 efs, prim_step e σ1 e2 σ2 efs -> σ1=σ2 /\ e'=e2 /\ efs = []) :
    ▷ ({E,E;Δ;Γ} ⊨ fill K' e' ≤log≤ t : τ)
    ⊢ {E,E;Δ;Γ} ⊨ fill K' e ≤log≤ t : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "IH".
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj /=".
    rewrite /env_subst.
    rewrite fill_subst. iModIntro.
    assert (Hsafe : ∀ σ, reducible (subst_p (fst <$> vvs) e) σ).
    { intros; apply subst_p_safe; eauto. }
    assert (to_val (subst_p (fst <$> vvs) e) = None) as Hval.
    { destruct (Hsafe ∅) as [e2 [σ2 [efs Hs]]].
      pose (Hv := val_prim_stuck). simpl in Hv.
      by eapply Hv. (* TODO: can we do this without poseing? *) }
    iApply (wp_bind (subst_ctx (fst <$> vvs) K')).
    iApply wp_lift_pure_det_step; eauto.
    { apply subst_p_det; eauto.
      - intros σ. destruct (Hsafe' σ) as (e2' & σ2' & efs & Hsafez).
        destruct (Hdet' _ _ _ _ Hsafez) as (?&?&?).
        simplify_eq. apply Hsafez.
      - intros ? ? ? ? Hs.
        destruct (Hdet' _ _ _ _ Hs) as (?&?&?).
        by simplify_eq. }
    iModIntro. iNext. iModIntro. simpl; iSplitL; last done.
    iSpecialize ("IH" with "Hs [HΓ] Hj"); auto. simpl.
    rewrite /env_subst fill_subst.
    iApply wp_bind_inv. 
    { (* TODO: how to get rid of this? *)
      change lang with (ectx_lang expr). 
      change fill with (ectx_language.fill).
      eapply ectx_lang_ctx. }
    iApply fupd_wp. by iApply (fupd_mask_mono E).
  Qed.

  Lemma bin_log_pure_masked_l (Δ : list D) (Γ : stringmap type) (E1 E2 : coPset)
    (K' : list ectx_item) (e e' t : expr) (τ : type)
    (Hsafe' : ∀ σ, reducible e σ)
    (Hdet': ∀ σ1 e2 σ2 efs, prim_step e σ1 e2 σ2 efs -> σ1=σ2 /\ e'=e2 /\ efs = []) :
    {E1,E2;Δ;Γ} ⊨ fill K' e' ≤log≤ t : τ
    ⊢ {E1,E2;Δ;Γ} ⊨ fill K' e ≤log≤ t : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "IH".
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj /=".
    rewrite /env_subst fill_subst.
    assert (Hsafe : ∀ σ, reducible (subst_p (fst <$> vvs) e) σ).
    { intros. by eapply subst_p_safe. }
    assert (to_val (subst_p (fst <$> vvs) e) = None) as Hval.
    { destruct (Hsafe ∅) as [e2 [σ2 [efs Hs]]].
      pose (Hv := val_prim_stuck). simpl in Hv.
      by eapply Hv. (* TODO: can we do this without poseing? *) }
    iApply (wp_bind (subst_ctx _ K')). 
    iMod ("IH" with "Hs [HΓ] Hj") as "IH"; auto. 
    iModIntro.    
    iApply wp_lift_pure_det_step; eauto.
    { apply subst_p_det; eauto.
      - intros σ. destruct (Hsafe' σ) as (e2' & σ2' & efs & Hsafez).
        destruct (Hdet' _ _ _ _ Hsafez) as (?&?&?).
        simplify_eq. apply Hsafez.
      - intros ? ? ? ? Hs.
        destruct (Hdet' _ _ _ _ Hs) as (?&?&?).
        by simplify_eq. }
    iModIntro. iNext. iModIntro. iSplitL; last done.
    rewrite /env_subst fill_subst /=.
    iApply wp_bind_inv. 
    { (* TODO: how to get rid of this? *)
      change lang with (ectx_lang expr). 
      change fill with (ectx_language.fill).
      eapply ectx_lang_ctx. }
    done.
  Qed.
 
  (* TODO: I have to 'refine' here for some reason *)
  Local Ltac solve_red H :=
    iApply (bin_log_pure_l with H); auto;
    [ intros; apply pure_exec_safe 
    | intros ???? Hst;
      refine (@pure_exec_puredet lang _ _ _ _ _ _ _ _ _ Hst)
    ]; eauto.
  Local Ltac solve_red_masked H :=
    iApply (bin_log_pure_masked_l with H); auto;
    [ intros; apply pure_exec_safe 
    | intros ???? Hst;
      refine (@pure_exec_puredet lang _ _ _ _ _ _ _ _ _ Hst)
    ]; eauto.

  Lemma bin_log_related_fst_l Δ Γ E K v1 v2 e τ :
   ▷ ({E,E;Δ;Γ} ⊨ fill K (of_val v1) ≤log≤ e : τ)
   ⊢ {E,E;Δ;Γ} ⊨ fill K (Fst (Pair (of_val v1) (of_val v2))) ≤log≤ e : τ.
  Proof.
    iIntros "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_snd_l Δ Γ E K v1 v2 e τ :
   ▷ ({E,E;Δ;Γ} ⊨ (fill K (of_val v2)) ≤log≤ e : τ)
   ⊢ {E,E;Δ;Γ} ⊨ (fill K (Snd (Pair (of_val v1) (of_val v2)))) ≤log≤ e : τ.
  Proof.
    iIntros "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_rec_l Δ Γ E K (f x : binder) e e' v t τ
   (Hclosed : Closed (x :b: f :b: ∅) e) :
   (to_val e' = Some v) →
   ▷ ({E,E;Δ;Γ} ⊨ (fill K (subst' f (Rec f x e) (subst' x e' e))) ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ (fill K (App (Rec f x e) e')) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_tlam_l Δ Γ E K e t τ
   (Hclosed : Closed ∅ e) :
   ▷ ({E,E;Δ;Γ} ⊨ fill K e ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ (fill K (TApp (TLam e))) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_fold_l Δ Γ E K e v t τ :
   (to_val e = Some v) →
   ▷ ({E,E;Δ;Γ} ⊨ fill K e ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ (fill K (Unfold (Fold e))) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    solve_red "Hlog".
  Qed.
  
  Lemma bin_log_related_pack_l Δ Γ E K e e' v t τ :
   to_val e = Some v →
   ▷ ({E,E;Δ;Γ} ⊨ fill K (App e' e) ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ fill K (Unpack (Pack e) e') ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_case_inl_l Δ Γ E K e v e1 e2 t τ :
   to_val e = Some v →
   ▷ ({E,E;Δ;Γ} ⊨ fill K (App e1 e) ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ fill K (Case (InjL e) e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_case_inr_l Δ Γ E K e v e1 e2 t τ :
   to_val e = Some v →
   ▷ ({E,E;Δ;Γ} ⊨ fill K (App e2 e) ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ fill K (Case (InjR e) e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_if_true_l Δ Γ E K e1 e2 t τ :
   ▷ ({E,E;Δ;Γ} ⊨ fill K e1 ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ fill K (If #true e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_if_true_masked_l Δ Γ E1 E2 K e1 e2 t τ :
   ({E1,E2;Δ;Γ} ⊨ fill K e1 ≤log≤ t : τ)
   ⊢ {E1,E2;Δ;Γ} ⊨ fill K (If #true e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    solve_red_masked "Hlog".
  Qed.

  Lemma bin_log_related_if_false_l Δ Γ E K e1 e2 t τ :
   ▷ ({E,E;Δ;Γ} ⊨ fill K e2 ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ (fill K (If #false e1 e2)) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    solve_red "Hlog".
  Qed.

  Lemma bin_log_related_if_false_masked_l Δ Γ E1 E2 K e1 e2 t τ :
   ({E1,E2;Δ;Γ} ⊨ fill K e2 ≤log≤ t : τ)
   ⊢ {E1,E2;Δ;Γ} ⊨ (fill K (If #false e1 e2)) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    solve_red_masked "Hlog".
  Qed.

  Lemma bin_log_related_binop_l Δ Γ E K op e1 e2 v1 v2 v t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    binop_eval op v1 v2 = Some v →
   ▷ ({E,E;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ)
   ⊢ {E,E;Δ;Γ} ⊨ (fill K (BinOp op e1 e2)) ≤log≤ t : τ.
  Proof.
    iIntros (???) "Hlog".
    solve_red "Hlog".
  Qed.

  (** *** Stateful reductions *)
  Lemma bin_log_related_wp_l Δ Γ E K e1 e2 τ
   (Hclosed1 : Closed ∅ e1) :
   (WP e1 @ E {{ v,
     {E,E;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ e2 : τ }})%I -∗
   {E,E;Δ;Γ} ⊨ fill K e1 ≤log≤ e2 : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "He".
    iIntros (vvs ρ) "#Hs #HΓ". iIntros (j K') "Hj /=".
    rewrite /env_subst fill_subst /=.
    rewrite Closed_subst_p_id. iModIntro.
    iApply (wp_bind (subst_ctx _ K)).
    iApply (wp_mask_mono E); auto.
    iApply (wp_wand with "He").
    iIntros (v) "Hv".
    iApply fupd_wp. iApply (fupd_mask_mono E); auto.
    iMod ("Hv" with "Hs [HΓ] Hj"); auto.
    rewrite /env_subst fill_subst /=. rewrite of_val_subst_p.
    done.
  Qed.

  Lemma bin_log_related_wp_atomic_l Δ Γ (E1 E2 : coPset) K e1 e2 τ
   (Hatomic : atomic e1)
   (Hclosed1 : Closed ∅ e1) :
   (|={E1,E2}=> WP e1 @ E2 {{ v,
     {E2,E1;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ e2 : τ }})%I -∗
   {E1,E1;Δ;Γ} ⊨ fill K e1 ≤log≤ e2 : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "Hlog".
    iIntros (vvs ρ) "#Hs #HΓ". iIntros (j K') "Hj /=". iModIntro.
    rewrite /env_subst fill_subst /=.
    rewrite Closed_subst_p_id.
    iApply (wp_bind (subst_ctx _ K)).
    iApply (wp_mask_mono E1); auto.
    iApply wp_atomic; auto.
    iMod "Hlog" as "He". iModIntro.
    iApply (wp_wand with "He").
    iIntros (v) "Hlog".
    iSpecialize ("Hlog" with "Hs [HΓ]"); first by iFrame.
    iSpecialize ("Hlog" with "Hj"). simpl.
    rewrite /env_subst fill_subst /=. rewrite of_val_subst_p.
    by iMod "Hlog".
  Qed.

  Lemma bin_log_related_step_atomic_l {A} (Φ : A → val → iProp Σ) Δ Γ E1 E2 K e1 e2 τ
    (Hatomic : atomic e1)
    (Hclosed1 : Closed ∅ e1) :
    (|={E1,E2}=> ∃ a : A, WP e1 @ E2 {{ v, Φ a v }} ∗
    (∀ v, Φ a v -∗ {E2,E1;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ e2 : τ)) -∗
    {E1,E1;Δ;Γ} ⊨ fill K e1 ≤log≤ e2 : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (a) "[He Hlog]". iModIntro.
    iApply (wp_wand with "He").
    iIntros (v) "HΦ".
    iApply ("Hlog" with "HΦ").
  Qed.

  (* TODO: simplify this monstrosity *)
  Ltac rewrite_closed :=
    try (let F := fresh in
      intro F); simpl;
       repeat match goal with
        | [ |- Closed ∅ _] => rewrite /Closed; cbn
        | [ |- Is_true (is_closed ∅ (of_val ?v))] => eapply of_val_closed
        | [ |- Is_true (is_closed _ _ && is_closed _ _)]
          => split_and?        
        | [Hval : to_val ?e = Some ?v |- context[is_closed ∅ ?e] ] 
          => rewrite -?(of_to_val e v Hval); eauto
        | [Hval : to_val ?e = Some ?v |- Is_true (is_closed ∅ ?e) ]
          => rewrite -?(of_to_val e v Hval); eauto
        | [Hcl : Closed ∅ ?e |- context[env_subst _ ?e]]
          => rewrite /env_subst Closed_subst_p_id
        | [Hcl : Closed ∅ ?e |- context[subst_p _ ?e]]
          => rewrite Closed_subst_p_id
        end;
      try done.

  Lemma bin_log_related_fork_l Δ Γ E1 E2 K (e t : expr) (τ : type)
   (Hclosed : Closed ∅ e) :
   (|={E1,E2}=> ▷ (WP e {{ _, True }}) ∗
    {E2,E1;Δ;Γ} ⊨ fill K #() ≤log≤ t : τ) -∗
   {E1,E1;Δ;Γ} ⊨ fill K (Fork e) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as "[Hsafe Hlog]". iModIntro.
    iApply wp_fork. iNext. by iFrame "Hsafe".
  Qed.

  Lemma bin_log_related_alloc_l Δ Γ E1 E2 K e v t τ
    (Heval : to_val e = Some v) :
    (|={E1,E2}=> ▷ (∀ l : loc, l ↦ᵢ v -∗
           {E2,E1;Δ;Γ} ⊨ fill K (# l) ≤log≤ t : τ))%I
    -∗ {E1,E1;Δ;Γ} ⊨ fill K (ref e) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog". iModIntro. 
    iApply (wp_alloc _ _ v); auto.
  Qed.

  Lemma bin_log_related_alloc_l' Δ Γ E K e v t τ
    (Heval : to_val e = Some v) :
    ▷ (∀ (l : loc), l ↦ᵢ v -∗ {E,E;Δ;Γ} ⊨ fill K (# l) ≤log≤ t : τ)%I
    -∗ {E,E;Δ;Γ} ⊨ fill K (ref e) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_related_alloc_l); auto. assumption.
  Qed.

  Lemma bin_log_related_load_l Δ Γ E1 E2 K l t τ :
    (|={E1,E2}=> ∃ v',
      ▷(l ↦ᵢ v') ∗
      ▷(l ↦ᵢ v' -∗ ({ E2,E1;Δ;Γ } ⊨ fill K (of_val v') ≤log≤ t : τ)))%I
    -∗ {E1,E1;Δ;Γ} ⊨ fill K (! #l) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v') "[Hl Hlog]". iModIntro.
    iApply (wp_load with "Hl"); auto.
  Qed.

  Lemma bin_log_related_load_l' Δ Γ E1 K l v' t τ :
    ▷ l ↦ᵢ v' -∗
    ▷ (l ↦ᵢ v' -∗ ({E1,E1;Δ;Γ} ⊨ fill K (of_val v') ≤log≤ t : τ))
    -∗ {E1,E1;Δ;Γ} ⊨ fill K !#l ≤log≤ t : τ.
  Proof.
    iIntros "Hl Hlog".
    iApply (bin_log_related_load_l); auto.
    iExists v'.
    iModIntro.
    by iFrame.
  Qed.

  Lemma bin_log_related_store_l Δ Γ E1 E2 K l e v' t τ :
    to_val e = Some v' →
    (|={E1,E2}=> ∃ v, ▷ l ↦ᵢ v ∗
      ▷(l ↦ᵢ v' -∗ {E2,E1;Δ;Γ} ⊨ fill K #() ≤log≤ t : τ))
    -∗ {E1,E1;Δ;Γ} ⊨ fill K (#l <- e) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v) "[Hl Hlog]". iModIntro.
    iApply (wp_store _ _ _ _ v' with "Hl"); auto.
  Qed.    

  Lemma bin_log_related_store_l' Δ Γ E K l e v v' t τ :
    (to_val e = Some v') →
    ▷ (l ↦ᵢ v) -∗
    ▷ (l ↦ᵢ v' -∗ {E,E;Δ;Γ} ⊨ fill K #() ≤log≤ t : τ)
    -∗ {E,E;Δ;Γ} ⊨ fill K (#l <- e) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hl Hlog".
    iApply (bin_log_related_store_l _ _ _ _ _ l e v'); auto.
    iModIntro. iExists v. iFrame.
  Qed.

  Lemma bin_log_related_cas_l Δ Γ E1 E2 K l e1 e2 v1 v2 t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    (|={E1,E2}=> ∃ v', ▷ l ↦ᵢ v' ∗ 
     (⌜v' ≠ v1⌝ -∗ ▷ (l ↦ᵢ v' -∗ {E2,E1;Δ;Γ} ⊨ fill K #false ≤log≤ t : τ)) ∧
     (⌜v' = v1⌝ -∗ ▷ (l ↦ᵢ v2 -∗ {E2,E1;Δ;Γ} ⊨ fill K #true ≤log≤ t : τ)))
    -∗ {E1,E1;Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (??) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v') "[Hl Hlog]". iModIntro.
    destruct (decide (v' = v1)).
    - (* CAS successful *) subst.
      iApply (wp_cas_suc with "Hl"); eauto.
      iDestruct "Hlog" as "[_ Hlog]".
      iSpecialize ("Hlog" with "[]"); eauto.
    - (* CAS failed *)
      iApply (wp_cas_fail with "Hl"); eauto.
      iDestruct "Hlog" as "[Hlog _]".
      iSpecialize ("Hlog" with "[]"); eauto.
  Qed.

  Lemma bin_log_related_cas_fail_l Δ Γ E1 E2 K l e1 e2 v1 v2 t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    (|={E1,E2}=> ∃ v', ▷ l ↦ᵢ v' ∗ ⌜v' ≠ v1⌝ ∗
     (l ↦ᵢ v' -∗ {E2,E1;Δ;Γ} ⊨ fill K #false ≤log≤ t : τ))
    -∗ {E1,E1;Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (??) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v') "[Hl [% Hlog]]". iModIntro. 
    iApply (wp_cas_fail with "Hl"); eauto.
  Qed.
  
  Lemma bin_log_related_cas_fail_l' Δ Γ E K l e1 e2 v1 v2 v' t τ :
    (to_val e1 = Some v1) →
    (to_val e2 = Some v2) →
    (v' ≠ v1) →
    ▷ (l ↦ᵢ v') -∗
    (l ↦ᵢ v' -∗ {E,E;Δ;Γ} ⊨ fill K #false ≤log≤ t : τ)
    -∗ {E,E;Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (???) "Hl Hlog".
    iApply bin_log_related_cas_fail_l; eauto.
    iModIntro. iExists v'. iFrame "Hl Hlog". eauto.
  Qed.

  Lemma bin_log_related_cas_suc_l Δ Γ E1 E2 K l e1 e2 v1 v2 t τ :
    (to_val e1 = Some v1) →
    (to_val e2 = Some v2) →
    (|={E1,E2}=> ▷ (l ↦ᵢ v1) ∗
     (l ↦ᵢ v2 -∗ {E2,E1;Δ;Γ} ⊨ fill K #true ≤log≤ t : τ))
    -∗ {E1,E1;Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.  
    iIntros (??) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as "[Hl Hlog]". iModIntro. 
    iApply (wp_cas_suc with "Hl"); eauto.
  Qed.

  Lemma bin_log_related_cas_suc_l' Δ Γ E K l e1 e2 v1 v2 v' t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v' = v1 →
    ▷ l ↦ᵢ v' -∗
    (l ↦ᵢ v2 -∗ {E,E;Δ;Γ} ⊨ fill K #true ≤log≤ t : τ)
    -∗ {E,E;Δ;Γ} ⊨ fill K (CAS (# l) e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (???) "Hl Hlog". subst.
    iApply bin_log_related_cas_suc_l; eauto.
    iModIntro. iFrame.
  Qed.
  
  (** ** Reductions on the right *)
  (** Lifting of the pure reductions *)
  Lemma bin_log_pure_r Δ Γ E1 E2 K' e e' t τ
    (Hspec : nclose specN ⊆ E1) :
    (∀ σ, prim_step e σ e' σ []) →
    {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K' e' : τ
    ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K' e : τ.
  Proof.
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros (Hstep) "Hlog".
    iIntros (vvs ρ) "#Hs #HΓ".
    iIntros (j K) "Hj /=".
    assert (Hsafe : ∀ σ, prim_step (subst_p (snd <$> vvs) e) σ (subst_p (snd <$> vvs) e') σ []).
    { intros.
      rewrite -(fmap_nil (subst_p (snd <$> vvs))).
      by apply subst_p_prim_step. }
    rewrite /env_subst fill_subst -fill_app.
    iMod (step_pure _ _ _ _ (subst_p _ e) (env_subst (snd <$> vvs) e') with "[Hs Hj]") as "Hj"; eauto.
    rewrite fill_app -fill_subst.
    iDestruct ("Hlog" with "Hs [HΓ] Hj") as "Hlog"; auto.    
  Qed.

  Lemma bin_log_pure_head_step_r Δ Γ E1 E2 K' e e' t τ
    (Hspec : nclose specN ⊆ E1) :
    (∀ σ, head_step e σ e' σ []) →
    {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K' e' : τ
    ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K' e : τ.
  Proof.
    intros Hst. apply bin_log_pure_r; eauto.
    intros. apply head_prim_step. apply Hst.
  Qed.

  Lemma bin_log_related_rec_r Δ Γ E1 E2 K f x e e' t v' τ
   (Hspec : nclose specN ⊆ E1)
   (Hclosed : Closed (x :b: f :b: ∅) e) :
   (to_val e' = Some v') →
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (subst' f (Rec f x e) (subst' x e' e)) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (App (Rec f x e) e') : τ.
  Proof.
    iIntros (?) "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    econstructor; eauto.
  Qed.

  Lemma bin_log_related_tlam_r Δ Γ E1 E2 K e t τ
   (Hclosed : Closed ∅ e)
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K e : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (TApp (TLam e)) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_fold_r Δ Γ E1 E2 K e v t τ
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K e : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Unfold (Fold e)) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.
  
  Lemma bin_log_related_pack_r Δ Γ E1 E2 K e e' v t τ
   (Hclosed : Closed ∅ e')
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (App e' e) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Unpack (Pack e) e') : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_fst_r Δ Γ E1 E2 K e v1 v2 τ
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K (of_val v1)) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K (Fst (Pair (of_val v1) (of_val v2)))) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_snd_r Δ Γ E1 E2 K e v1 v2 τ
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K (of_val v2)) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K (Snd (Pair (of_val v1) (of_val v2)))) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_case_inl_r Δ Γ E1 E2 K e v e1 e2 t τ
   (Hclosed1 : Closed ∅ e1) 
   (Hclosed2 : Closed ∅ e2)   
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ (fill K (App e1 e)) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ (fill K (Case (InjL e) e1 e2)) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_case_inr_r Δ Γ E1 E2 K e v e1 e2 t τ
   (Hclosed1 : Closed ∅ e1) 
   (Hclosed2 : Closed ∅ e2)   
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E1) :
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ (fill K (App e2 e)) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ (fill K (Case (InjR e) e1 e2)) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_if_true_r Δ Γ E1 E2 K e e1 e2 τ
   (Hspec : nclose specN ⊆ E1)
   (Hclosed1 : Closed ∅ e1)
   (Hclosed2 : Closed ∅ e2) :
   {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K e1) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K (If #true e1 e2)) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_if_false_r Δ Γ E1 E2 K e e1 e2 τ
   (Hspec : nclose specN ⊆ E1)
   (Hclosed1 : Closed ∅ e1)
   (Hclosed2 : Closed ∅ e2) :
   {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K e2) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ e ≤log≤ (fill K (If #false e1 e2)) : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.

  Lemma bin_log_related_binop_r Δ Γ E1 E2 K op e1 e2 v1 v2 v t τ
   (Hspec : nclose specN ⊆ E1) :
   to_val e1 = Some v1 →
   to_val e2 = Some v2 →
   binop_eval op v1 v2 = Some v →
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (of_val v) : τ
   ⊢ {E1,E2;Δ;Γ} ⊨ t ≤log≤ (fill K (BinOp op e1 e2)) : τ.
  Proof.
    iIntros (???) "Hlog".
    iApply (bin_log_pure_head_step_r with "Hlog"); auto; intros.
    { econstructor; eauto. }
  Qed.
  
  (** Stateful reductions *)
  Lemma bin_log_related_step_r Φ Δ Γ E1 E2 K' e1 e2 τ
    (Hclosed2 : Closed ∅ e2) :
    (∀ ρ j K, spec_ctx ρ -∗ (j ⤇ fill K e2 ={E1}=∗ ∃ v, j ⤇ fill K (of_val v)
                  ∗ Φ v)) -∗
    (∀ v, Φ v -∗ {E1,E2;Δ;Γ} ⊨ e1 ≤log≤ fill K' (of_val v) : τ) -∗
    {E1,E2;Δ;Γ} ⊨ e1 ≤log≤ fill K' e2 : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "He Hlog".
    iIntros (vvs ρ) "#Hs #HΓ". iIntros (j K) "Hj /=".
    rewrite /env_subst fill_subst /= -fill_app.
    rewrite_closed.
    iMod ("He" $! ρ j with "Hs Hj") as (v) "[Hj Hv]".    
    iSpecialize ("Hlog" $! v with "Hv Hs [HΓ]"); first by iFrame.
    rewrite /env_subst fill_app fill_subst.
    rewrite of_val_subst_p /=.
    iSpecialize ("Hlog" with "Hj"); simpl.
    done.
  Qed.

  Lemma bin_log_related_fork_r Δ Γ E1 E2 K (e t : expr) (τ : type)
   (Hmasked : nclose specN ⊆ E1)
   (Hclosed : Closed ∅ e) :
   (∀ i, i ⤇ e -∗
     {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K #() : τ) -∗
   {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Fork e) : τ.
  Proof.
    iIntros "Hlog".
    pose (Φ := (fun (v : val) => ∃ i, i ⤇ e ∗ ⌜v = #()⌝%V)%I).
    iApply (bin_log_related_step_r Φ with "[]"); cbv[Φ].
    { iIntros (ρ j K') "#Hspec Hj".
      tp_fork j as i "Hi".
      iModIntro. iExists #(). iFrame. eauto.
    }
    iIntros (v). iDestruct 1 as (i) "[Hi %]"; subst.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_alloc_r Δ Γ E1 E2 K e v t τ
    (Hmasked : nclose specN ⊆ E1)
    (Heval : to_val e = Some v) :
    (∀ l : loc, l ↦ₛ v -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K #l : τ)%I
    -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Alloc e) : τ.
  Proof.
    iIntros "Hlog".
    pose (Φ := (fun w => ∃ l : loc, ⌜w = (# l)⌝ ∗ l ↦ₛ v)%I).
    iApply (bin_log_related_step_r Φ with "[]").
    { cbv[Φ]. 
      iIntros (ρ j K') "#Hs Hj /=".
      tp_alloc j as l "Hl". iExists (# l).
      iFrame. iExists l. eauto. }
    iIntros (v') "He'". iDestruct "He'" as (l) "[% Hl]". subst.
    iApply "Hlog".
    done.
  Qed.

  Lemma bin_log_related_load_r Δ Γ E1 E2 K l v' t τ
    (Hmasked : nclose specN ⊆ E1) :
    l ↦ₛ v' -∗
    (l ↦ₛ v' -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (of_val v') : τ)
    -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Load (# l)) : τ.
  Proof.
    iIntros "Hl Hlog".
    pose (Φ := (fun w => ⌜w = v'⌝ ∗ l ↦ₛ v')%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=". iExists v'.
      tp_load j.
      iFrame. eauto. }
    iIntros (v) "[% Hl]"; subst.
    iApply "Hlog".
    done.
  Qed.

  Lemma bin_log_related_store_r Δ Γ E1 E2 K l e v v' t τ 
    (Hmasked : nclose specN ⊆ E1) :
    to_val e = Some v' →
    l ↦ₛ v -∗
    (l ↦ₛ v' -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (#()) : τ)
    -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (#l <- e) : τ.
  Proof.
    iIntros (?) "Hl Hlog".
    pose (Φ := (fun w => ⌜w = #()⌝ ∗ l ↦ₛ v')%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=". iExists #().
      tp_store j.
      iFrame. eauto. }
    iIntros (w) "[% Hl]"; subst.
    iApply "Hlog".
    done.
  Qed.

  Lemma bin_log_related_cas_fail_r Δ Γ E1 E2 K l e1 e2 v1 v2 v t τ :
    nclose specN ⊆ E1 →
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v ≠ v1 →
    l ↦ₛ v -∗
    (l ↦ₛ v -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K #false : τ)
    -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (CAS #l e1 e2) : τ.
  Proof.
    iIntros (????) "Hl Hlog".
    pose (Φ := (fun (w : val) => ⌜w = #false⌝ ∗ l ↦ₛ v)%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=".
      tp_cas_fail j; auto.
      iExists #false. simpl.
      iFrame. eauto. }
    iIntros (w) "[% Hl]"; subst.
    iApply "Hlog".
    done.
  Qed.

  Lemma bin_log_related_cas_suc_r Δ Γ E1 E2 K l e1 e2 v1 v2 v t τ :
    nclose specN ⊆ E1 →   
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v = v1 →
    l ↦ₛ v -∗
    (l ↦ₛ v2 -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K #true : τ)
    -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (CAS #l e1 e2) : τ.
  Proof.
    iIntros (????) "Hl Hlog".
    pose (Φ := (fun w => ⌜w = #true⌝ ∗ l ↦ₛ v2)%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=".
      tp_cas_suc j; auto.
      iExists #true. simpl.
      iFrame. eauto. }
    iIntros (w) "[% Hl]"; subst.
    iApply "Hlog".
    done.
  Qed.
End properties.
