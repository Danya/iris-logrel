(* Some refinement from the paper
   "The effects of higher-order state and control on local relational reasoning"
   D. Dreyer, G. Neis, L. Birkedal
*)
From iris.proofmode Require Import tactics.
From iris.algebra Require Import csum agree excl.
From iris_logrel Require Import logrel.

Lemma bin_log_related_seq `{logrelG Σ} τ1 E Δ Γ e1 e2 e1' e2' τ2
`{Closed ∅ e2} `{Closed ∅ e2'} :
  ↑logrelN ⊆ E →
  {E,E;Δ;Γ} ⊨ e1 ≤log≤ e1' : τ1 -∗
  {E,E;Δ;Γ} ⊨ e2 ≤log≤ e2' : τ2 -∗
  {E,E;Δ;Γ} ⊨ (e1;; e2) ≤log≤ (e1';; e2') : τ2.
Proof.
  iIntros (?) "He1 He2".
  rel_bind_l e1.
  rel_bind_r e1'.
  iApply (related_bind with "He1 [He2]").
  iIntros (?) "? /=".
  rel_rec_l.
  rel_rec_r.
  done.
Qed.

Section refinement.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).

  Lemma refinement1 Γ :
    Γ ⊨
      let: "x" := ref #1 in
      (λ: "f", "f" #();; !"x")
    ≤log≤
      (λ: "f", "f" #();; #1)
    : TArrow (TArrow TUnit TUnit) TNat.
  Proof.
  iIntros (Δ).
  rel_alloc_l as x "Hx".
  iMod (inv_alloc (nroot.@"xinv") _ (x ↦ᵢ #1)%I with "Hx") as "#Hinv".
  rel_let_l.
  iApply bin_log_related_arrow; auto.
  iIntros "!#" (f1 f2) "Hf".
  rel_let_l. rel_let_r.
  iApply (bin_log_related_seq with "[Hf]"); auto.
  - iApply (bin_log_related_app with "Hf").
    by rel_vals.
  - rel_load_l_atomic.
    iInv (nroot.@"xinv") as "Hx" "Hcl".
    iModIntro. iExists _; iFrame "Hx".
    iNext. iIntros "Hx".
    iMod ("Hcl" with "Hx").
    rel_vals; eauto.
  Qed.

  Definition oneshotR := csumR (exclR unitR) (agreeR unitR).
  Class oneshotG Σ := { oneshot_inG :> inG Σ oneshotR }.
  Definition oneshotΣ : gFunctors := #[GFunctor oneshotR].
  Instance subG_oneshotΣ {Σ} : subG oneshotΣ Σ → oneshotG Σ.
  Proof. solve_inG. Qed.

  Definition pending γ `{oneshotG Σ} := own γ (Cinl (Excl ())).
  Definition shot γ `{oneshotG Σ} := own γ (Cinr (to_agree ())).
  Lemma new_pending `{oneshotG Σ} : (|==> ∃ γ, pending γ)%I.
  Proof. by apply own_alloc. Qed.
  Lemma shoot γ `{oneshotG Σ} : pending γ ==∗ shot γ.
  Proof.
    apply own_update.
    intros n [f |]; simpl; eauto.
    destruct f; simpl; try by inversion 1.
  Qed.
  Definition shootN := nroot .@ "shootN".
  Lemma shot_not_pending γ `{oneshotG Σ} :
    shot γ -∗ pending γ -∗ False.
  Proof.
    iIntros "Hs Hp".
    iPoseProof (own_valid_2 with "Hs Hp") as "H".
    iDestruct "H" as %[].
  Qed.

  Lemma refinement2 `{oneshotG Σ} Γ :
    Γ ⊨
      let: "x" := ref #0 in
      (λ: "f", "x" <- #1;; "f" #();; !"x")
    ≤log≤
      (let: "x" := ref #1 in
       λ: "f", "f" #();; !"x")
    : TArrow (TArrow TUnit TUnit) TNat.
  Proof.
    iIntros (Δ).
    rel_alloc_l as x "Hx".
    rel_alloc_r as y "Hy".
    rel_let_l; rel_let_r.
    iApply fupd_logrel.
    iMod new_pending as (γ) "Ht". (*TODO typeclasses for this?*)
    iModIntro.
    iMod (inv_alloc shootN _ ((x ↦ᵢ #0 ∗ pending γ ∨ x ↦ᵢ #1 ∗ shot γ) ∗ y ↦ₛ #1)%I with "[Hx Ht $Hy]") as "#Hinv".
    { iNext. iLeft. iFrame. }
    iApply bin_log_related_arrow; auto.
    iIntros "!#" (f1 f2) "Hf".
    rel_let_l. rel_let_r.
    rel_store_l_atomic.
    iInv shootN as "[[[Hx Hp] | [Hx #Hs]] Hy]" "Hcl";
      iModIntro; iExists _; iFrame "Hx"; iNext; iIntros "Hx"; rel_rec_l.
    - iApply fupd_logrel'.
      iMod (shoot γ with "Hp") as "#Hs".
      iModIntro.
      iMod ("Hcl" with "[$Hy Hx]") as "_".
      { iNext. iRight. by iFrame. }
      iApply (bin_log_related_seq with "[Hf]"); auto.
      + iApply (bin_log_related_app with "Hf").
        by rel_vals.
      + rel_load_l_atomic.
        iInv shootN as "[[[Hx >Hp] | [Hx Hs']] Hy]" "Hcl".
        { iExFalso. iApply (shot_not_pending with "Hs Hp"). }
        iModIntro. iExists _; iFrame. iNext.
        iIntros "Hx".
        rel_load_r.
        iMod ("Hcl" with "[-]").
        { iNext. iFrame. iRight; by iFrame. }
        rel_vals; eauto.
    - iMod ("Hcl" with "[$Hy Hx]") as "_".
      { iNext. iRight. by iFrame. }
      iApply (bin_log_related_seq with "[Hf]"); auto.
      + iApply (bin_log_related_app with "Hf").
        by rel_vals.
      + rel_load_l_atomic.
        iInv shootN as "[[[Hx >Hp] | [Hx Hs']] Hy]" "Hcl".
        { iExFalso. iApply (shot_not_pending with "Hs Hp"). }
        iModIntro. iExists _; iFrame. iNext.
        iIntros "Hx".
        rel_load_r.
        iMod ("Hcl" with "[-]").
        { iNext. iFrame. iRight; by iFrame. }
        rel_vals; eauto.
  Qed.

  Definition i3 x x' b b' : iProp Σ :=
    ((∃ (n : nat), x ↦ᵢ #n ∗ x' ↦ₛ #n ∗
                 b ↦ᵢ #true ∗ b' ↦ₛ #true)
               ∨ (b ↦ᵢ #false ∗ b' ↦ₛ #false))%I.
  Definition i3n := nroot .@ "i3".
  Lemma refinement3  Γ :
    Γ ⊨
      let: "b" := ref #true in
      let: "x" := ref #0 in
      (λ: "f", if: CAS "b" #true #false
               then "f" #();; "x" <- !"x" + #1 ;; "b" <- #true
               else #())
    ≤log≤
      (let: "b" := ref #true in
      let: "x" := ref #0 in
      (λ: "f", if: CAS "b" #true #false
               then let: "n" := !"x" in
                    "f" #();; "x" <- "n" + #1 ;; "b" <- #true
               else #()))
    : TArrow (TArrow TUnit TUnit) TUnit.
  Proof.
    iIntros (Δ).
    rel_alloc_l as b "Hb".
    rel_let_l.
    rel_alloc_l as x "Hx".
    rel_let_l.
    rel_alloc_r as b' "Hb'".
    rel_let_r.
    rel_alloc_r as x' "Hx'".
    rel_let_r.
    iMod (inv_alloc i3n _ (i3 x x' b b') with "[-]") as "#Hinv".
    { iNext. unfold i3.
      iLeft. iExists 0. iFrame. }
    iApply bin_log_related_arrow; eauto.
    iAlways. iIntros (f f') "Hf".
    rel_let_l.
    rel_let_r.
    rel_cas_l_atomic.
    iInv i3n as ">Hbb" "Hcl".
    rewrite {2}/i3.
    iDestruct "Hbb" as "[Hbb | (Hb & Hb')]"; last first.
    { iModIntro; iExists _; iFrame.
      iSplitL; last by iIntros (?); congruence.
      iIntros (?); iNext; iIntros "Hb".
      rel_cas_fail_r; rel_if_r; rel_if_l.
      iMod ("Hcl" with "[-]").
      { iNext. iRight. iFrame. }
      rel_vals; eauto.
    }
    { iDestruct "Hbb" as (n) "(Hx & Hx' & Hb & Hb')".
      iModIntro. iExists _; iFrame.
      iSplitR; first by iIntros (?); congruence.
      iIntros (?); iNext; iIntros "Hb".
      rel_cas_suc_r; rel_if_r; rel_if_l.
      rel_load_r. rel_let_r.
      iMod ("Hcl" with "[Hb Hb']") as "_".
      { iNext. iRight. iFrame. }
      iApply (bin_log_related_seq with "[Hf]"); auto.
      { iApply (bin_log_related_app with "Hf").
        iApply bin_log_related_unit. }
      rel_load_l.
      rel_op_l. rel_op_r.
      rel_store_l. rel_store_r.
      rel_seq_l. rel_seq_r.
      rel_store_l_atomic.
      iInv i3n as ">Hi3" "Hcl".
      iDestruct "Hi3" as "[Hi3 | [Hb Hb']]".
      { iDestruct "Hi3" as (m) "(Hx1 & Hx1' & Hb & Hb')".
        iModIntro. iExists _; iFrame; iNext. iIntros "Hb".
        iDestruct (mapsto_valid_2 x with "Hx Hx1") as %Hfoo.
        cbv in Hfoo. by exfalso. }
      iModIntro; iExists _; iFrame; iNext; iIntros "Hb".
      rel_store_r.
      iMod ("Hcl" with "[-]") as "_".
      { iNext. iLeft. iExists _. iFrame. }
      rel_vals; eauto. }
  Qed.

  Definition bot : val := rec: "bot" <> := "bot" #().
  Lemma bot_l ϕ Δ Γ E K t τ :
    (ϕ -∗ {E,E;Δ;Γ} ⊨ fill K (bot #()) ≤log≤ t : τ) -∗
    {E,E;Δ;Γ} ⊨ fill K (bot #()) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iLöb as "IH".
    rel_rec_l.
    unlock bot; simpl_subst/=.
    iApply ("IH" with "Hlog").
  Qed.
        
End refinement.
