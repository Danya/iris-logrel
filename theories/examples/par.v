From iris.proofmode Require Import tactics.
From iris_logrel Require Export logrel.

Definition par : val := λ: "e1" "e2",
  let: "x1" := ref InjL #() in
  Fork ("x1" <- InjR ("e1" #()));;
  let: "x2" := "e2" #() in
  let: "f" := rec: "f" <> := 
    match: !"x1" with
      InjL <>  => "f" #()
    | InjR "v" => "v"
    end
  in ("f" #(), "x2").

Lemma par_type Γ τ1 τ2 :
  typed Γ par
        (TArrow (TArrow TUnit τ1) (TArrow (TArrow TUnit τ2)
            (TProd τ1 τ2))).
Proof. solve_typed. Qed.

Hint Resolve par_type : typeable.

Section compatibility.
  Context `{logrelG Σ}.

  Lemma bin_log_related_par Δ Γ E e1 e2 e1' e2' τ1 τ2 :
    ↑logrelN ⊆ E →
    {E,E;Δ;Γ} ⊨ e1 ≤log≤ e1' : TArrow TUnit τ1 -∗
    {E,E;Δ;Γ} ⊨ e2 ≤log≤ e2' : TArrow TUnit τ2 -∗
    {E,E;Δ;Γ} ⊨ par e1 e2 ≤log≤ par e1' e2' : TProd τ1 τ2.
  Proof.
    iIntros (?) "He1 He2".
    iApply (bin_log_related_app with "[He1] He2").
    iApply (bin_log_related_app with "[] He1").
    iApply binary_fundamental_masked; eauto with typeable.
  Qed.

End compatibility.
