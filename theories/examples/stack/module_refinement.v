From iris.proofmode Require Import tactics.
From iris_logrel Require Import logrel.
From iris_logrel.examples.stack Require Import
  CG_stack FG_stack stack_rules refinement.

Section Mod_refinement.
  Context `{HLR : logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types Δ : listC D.
  Import lang.

  Program Definition sint {LG : logrelG Σ} {Z : stackPreG Σ} τi : prodC valC valC -n> iProp Σ := λne vv,
    (∃ γ (l stk stk' : loc), ⌜(vv.2) = (#stk', #l)%V⌝ ∗ ⌜(vv.1) = #stk⌝
      ∗ inv (stackN .@ (stk,stk')) (sinv' γ τi stk stk' l))%I.
  Solve Obligations with solve_proper.

  Instance sint_persistent `{logrelG Σ, stackPreG Σ} τi vv : PersistentP (sint τi vv).
  Proof. apply _. Qed.
  
  Lemma module_refinement `{SPG : stackPreG Σ} Δ Γ :
    {⊤,⊤;Δ;Γ} ⊨ FG_stack.stackmod ≤log≤ CG_stack.stackmod : TForall $ TExists $ TProd (TProd
                                                 (TArrow TUnit (TVar 0))
                                                 (TArrow (TVar 0) (TSum TUnit (TVar 1))))
                                                 (TArrow (TVar 0) (TArrow (TVar 1) TUnit)).
  Proof.
    unlock FG_stack.stackmod CG_stack.stackmod.
    iApply bin_log_related_tlam; auto.
    iIntros (τi Hτi) "!#".
    iApply (bin_log_related_pack _ (sint τi)).
    do 3 rel_tlam_l.
    do 3 rel_tlam_r.
    repeat iApply bin_log_related_pair.
    - iApply bin_log_related_arrow; eauto.
      iAlways. iIntros (? ?) "_".
      rel_seq_l.
      rel_seq_r.
      rel_alloc_l as istk "Histk". simpl.
      rel_alloc_l as stk "Hstk".
      rel_alloc_r as stk' "Hstk'".
      rel_alloc_r as l "Hl".
      rel_vals.
      rewrite -persistentP.
      iMod (own_alloc (● (∅ : stackUR))) as (γ) "Hemp"; first done.
      pose (SG := StackG Σ _ γ).
      iAssert (prestack_owns γ ∅) with "[Hemp]" as "Hoe".
      { rewrite /prestack_owns big_sepM_empty fmap_empty.
        iFrame "Hemp". }
      iMod (stack_owns_alloc with "[$Hoe $Histk]") as "[Hoe #Histk]".
      iAssert (preStackLink γ τi (#istk, FoldV (InjLV (LitV Unit)))) with "[Histk]" as "#HLK".
      { rewrite preStackLink_unfold.
        iExists _, _. iSplitR; simpl; trivial.
        iFrame "Histk". iLeft. iSplit; trivial. }
      iAssert (sinv' γ τi stk stk' l) with "[Hoe Hstk Hstk' HLK Hl]" as "Hinv".
      { iExists _, _, _. by iFrame. }
      iMod (inv_alloc (stackN.@(stk,stk')) with "[Hinv]") as "#Hinv".
      { iNext. iExact "Hinv". }
      iModIntro.
      iExists γ, l, stk, stk'. eauto.
    - iApply bin_log_related_arrow_val; eauto.
      iAlways. iIntros (? ?) "#Hvv /=".
      iDestruct "Hvv" as (γ l stk stk') "(% & % & #Hinv)".
      simplify_eq/=.
      rel_let_l.
      rel_let_r.
      Transparent FG_pop CG_locked_pop.
      rel_rec_l.
      rel_proj_r; rel_rec_r.
      unlock CG_locked_pop. simpl_subst/=.
      rel_proj_r.
      rel_let_r.
      replace (#();; acquire # l ;; let: "v" := (CG_pop #stk') #() in release # l ;; "v")%E with ((CG_locked_pop $/ LitV (Loc stk') $/ LitV (Loc l)) #())%E; last first.
      { unlock CG_locked_pop. simpl_subst/=. reflexivity. }
      replace (TSum TUnit (TVar 1))
      with (TSum TUnit (TVar 0)).[ren (+1)] by done.
      iApply bin_log_related_weaken_2.
      pose (SG := StackG Σ _ γ).
      change γ with stack_name.
      iApply (FG_CG_pop_refinement' (stackN.@(stk,stk'))).
      { solve_ndisj. }
      by rewrite sinv_unfold.
    - iApply bin_log_related_arrow_val; eauto.
      { unlock FG_push. done. }
      iAlways. iIntros (? ?) "#Hvv /=".
      iDestruct "Hvv" as (γ l stk stk') "(% & % & #Hinv)".
      simplify_eq/=.
      rel_let_r.
      Transparent FG_push.
      rel_rec_l.
      iApply bin_log_related_arrow_val; eauto.
      { unlock FG_push. simpl_subst/=. done. }
      { unlock FG_push. simpl_subst/=. solve_closed. }
      iAlways. iIntros (v v') "#Hτi /=".
      rel_let_r.
      rel_proj_r; rel_rec_r.
      unlock CG_locked_push. simpl_subst/=.
      rel_proj_r; rel_let_r.
      replace (let: "x" := v' in acquire # l ;; (CG_push #stk') "x" ;; release # l)%E
        with ((CG_locked_push $/ LitV stk' $/ LitV l) v')%E; last first.
      { unlock CG_locked_push. simpl_subst/=. done. }
      change TUnit with (TUnit.[ren (+1)]).
      pose (SG := StackG Σ _ γ).
      change γ with stack_name.
      iApply (FG_CG_push_refinement (stackN.@(stk,stk')) with "[Hinv] Hτi").
      { solve_ndisj. }
      by rewrite sinv_unfold.
  Qed.
End Mod_refinement.

Theorem module_ctx_refinement :
  ∅ ⊨ FG_stack.stackmod ≤ctx≤ CG_stack.stackmod :
    TForall $ TExists $ TProd (TProd
                                 (TArrow TUnit (TVar 0))
                                 (TArrow (TVar 0) (TSum TUnit (TVar 1))))
                                 (TArrow (TVar 0) (TArrow (TVar 1) TUnit)).
Proof.
  set (Σ := #[logrelΣ; GFunctor (authR stackUR)]).
  eapply (logrel_ctxequiv Σ); [solve_closed.. | intros ].
  apply module_refinement.
Qed.
