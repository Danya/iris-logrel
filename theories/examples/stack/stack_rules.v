From iris.algebra Require Import gmap agree.
From iris.base_logic.lib Require Export auth.
From iris_logrel Require Export logrel.

Import uPred.

Definition stackUR : ucmraT := gmapUR loc (agreeR valC).

Class stackG Σ :=
  StackG { stack_inG :> authG Σ stackUR; stack_name : gname }.
Class stackPreG Σ := StackPreG { stack_pre_inG :> authG Σ stackUR }.
Definition stackΣ : gFunctors := #[authΣ stackUR].
Instance subG_stackPreG {Σ} : subG stackΣ Σ → stackPreG Σ.
Proof. solve_inG. Qed.
(* Instance stackG_stackPreG {Σ} : stackG Σ → stackPreG Σ. *)
(* Proof. intros [S ?]. by constructor. Qed. *)

Definition prestack_mapsto `{authG Σ stackUR} (γ : gname) (l : loc) (v : val) : iProp Σ :=
  own γ (◯ {[ l := to_agree v ]}).

Definition stack_mapsto `{stackG Σ} l v : iProp Σ := prestack_mapsto stack_name l v.

Notation "l ↦ˢᵗᵏ v" := (stack_mapsto l v) (at level 20) : uPred_scope.

Section Rules_pre.
  Context `{authG Σ stackUR}.
  Variable (γ : gname).
  Notation D := (prodC valC valC -n> iProp Σ).
  Notation "l ↦ˢᵗᵏ v" := (prestack_mapsto γ l v) (at level 20) : uPred_scope.

  Global Instance stack_mapsto_persistent l v : PersistentP (l ↦ˢᵗᵏ v).
  Proof. apply _. Qed.
  
  Lemma prestack_mapstos_agree_uncurried l v w : l ↦ˢᵗᵏ v ∗ l ↦ˢᵗᵏ w ⊢ ⌜v = w⌝.
  Proof.
    rewrite -own_op -auth_frag_op op_singleton.
    change (own γ (◯ {[l := to_agree v ⋅ to_agree w]}))
      with (auth_own γ {[l := to_agree v ⋅ to_agree w]}).
    rewrite auth_own_valid. iIntros "Hvalid". iDestruct "Hvalid" as %Hvalid.
    rewrite singleton_valid in Hvalid *.
    intros Hagree. by rewrite (agree_op_inv' v w Hagree).
  Qed.

  Lemma prestack_mapstos_agree l v w : l ↦ˢᵗᵏ v -∗ l ↦ˢᵗᵏ w -∗ ⌜v = w⌝.
  Proof.
    iIntros "??".
    iApply prestack_mapstos_agree_uncurried. by iFrame.
  Qed.
  
  (* stacklink Q := {((Loc l), nil) ∣ l ↦ˢᵗᵏ (InjL #()) }
          ∪  {((Loc l), cons y2 z2) ∣ ∃ y1 z1, l ↦ˢᵗᵏ (y1, z1) 
                                    ∗ (y1, y2) ∈ Q
                                    ∗ ▷ stacklink Q (z1, z2) }*)
  Program Definition preStackLink_pre (Q : D) : D -n> D := λne P v,
    (∃ (l : loc) w, ⌜v.1 = # l⌝ ∗ l ↦ˢᵗᵏ w ∗
            ((⌜w = InjLV #()⌝ ∧ ⌜v.2 = FoldV (InjLV #())⌝) ∨
            (∃ y1 z1 y2 z2, ⌜w = InjRV (PairV y1 (FoldV z1))⌝ ∗
              ⌜v.2 = FoldV (InjRV (PairV y2 z2))⌝ ∗ Q (y1, y2) ∗ ▷ P(z1, z2))))%I.
  Solve Obligations with solve_proper.

  Global Instance StackLink_pre_contractive Q : Contractive (preStackLink_pre Q).
  Proof. solve_contractive. Qed.

  Definition preStackLink (Q : D) : D := fixpoint (preStackLink_pre Q).

  Lemma preStackLink_unfold Q v :
    preStackLink Q v ≡ (∃ (l : loc) w,
      ⌜v.1 = # l⌝ ∗ l ↦ˢᵗᵏ w ∗
      ((⌜w = InjLV #()⌝ ∧ ⌜v.2 = FoldV (InjLV #())⌝) ∨
      (∃ y1 z1 y2 z2, ⌜w = InjRV (PairV y1 (FoldV z1))⌝
                      ∗ ⌜v.2 = FoldV (InjRV (PairV y2 z2))⌝
                      ∗ Q (y1, y2) ∗ ▷ preStackLink Q (z1, z2))))%I.
  Proof. by rewrite {1}/preStackLink fixpoint_unfold. Qed.

  Global Opaque preStackLink. (* So that we can only use the unfold above. *)

  Global Instance preStackLink_persistent (Q : D) v `{∀ vw, PersistentP (Q vw)} :
    PersistentP (preStackLink Q v).
  Proof.
    rewrite /PersistentP.
    iIntros "H". iLöb as "IH" forall (v). rewrite preStackLink_unfold.
    iDestruct "H" as (l w) "[% [#Hl [[% %]|Hr]]]"; subst.
    { iExists l, _; iAlways; eauto. }
    iDestruct "Hr" as (y1 z1 y2 z2) "[% [% [#HQ Hrec]]]"; subst.    
    rewrite later_forall. 
    iSpecialize ("IH" $! (z1, z2)). rewrite later_wand. 
    iSpecialize ("IH" with "Hrec"). rewrite -always_later.
    iDestruct "IH" as "#IH".
    iAlways. iExists _,_; eauto 20.
  Qed.

  Lemma stackR_alloc (h : stackUR) (i : loc) (v : val) :
    h !! i = None → ● h ~~> ● (<[i := to_agree v]> h) ⋅ ◯ {[i := to_agree v]}.
  Proof. intros. by apply auth_update_alloc, alloc_singleton_local_update. Qed.

  Definition prestack_owns `{logrelG Σ} (h : gmap loc val) : iProp Σ :=
    (own γ (● (to_agree <$> h : stackUR))
        ∗ [∗ map] l ↦ v ∈ h, l ↦ᵢ v)%I.

  Lemma prestack_owns_alloc `{logrelG Σ} h l v :
    prestack_owns h ∗ l ↦ᵢ v ==∗ prestack_owns (<[l := v]> h) ∗ l ↦ˢᵗᵏ v.
  Proof.
    iIntros "[[Hown Hall] Hl]".
    iDestruct (own_valid with "Hown") as %Hvalid.
    destruct (h !! l) as [av|] eqn:Hhl.
    { iDestruct (big_sepM_lookup _ h l with "Hall") as "Hl'"; first done.
      iDestruct (@mapsto_valid_2 loc val with "Hl Hl'") as %Hval.
      cbv in Hval. exfalso; by apply Hval. }
    { iMod (own_update with "Hown") as "[Hown Hl']".
      eapply auth_update_alloc.
      eapply (alloc_singleton_local_update _ l (to_agree v)).
      - rewrite lookup_fmap Hhl. by compute.
      - by compute.
      - iModIntro. rewrite /prestack_owns. rewrite fmap_insert.
        iFrame "Hown Hl'".
        iApply (big_sepM_insert _ h l); eauto.
        by iFrame. }
  Qed.

  Lemma prestack_owns_open_close `{logrelG Σ} h l v :
    prestack_owns h -∗ l ↦ˢᵗᵏ v -∗ l ↦ᵢ v ∗ (l ↦ᵢ v -∗ prestack_owns h).
  Proof.
    iIntros "[Howns Hls] Hl".
    iDestruct (own_valid_2 with "Howns Hl") 
      as %[[? [[av [Hav ?]]%equiv_Some_inv_r' Hav']]%singleton_included ?]%auth_valid_discrete_2.
    setoid_subst.
    (* TODO: ask Robbert why did I have to change this *)
    apply -> @Some_included_total in Hav'; [| apply _].
    move: Hav. rewrite lookup_fmap fmap_Some.
    move=> [v' [Hl Hav]]; subst.
    apply to_agree_included in Hav'; setoid_subst.
    iDestruct (big_sepM_lookup_acc _ h l with "Hls") as "[$ Hclose]"; first done.
    iIntros "Hl'". rewrite /prestack_owns. iFrame "Howns". by iApply "Hclose".
  Qed.

  Lemma prestack_owns_later_open_close `{logrelG Σ} h l v :
    ▷ prestack_owns h -∗ l ↦ˢᵗᵏ v -∗ ▷ (l ↦ᵢ v ∗ (l ↦ᵢ v -∗ prestack_owns h)).
  Proof. iIntros "H1 H2". iNext; by iApply (prestack_owns_open_close with "H1"). Qed.
End Rules_pre.

Section Rules.
  Context `{stackG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).

  Definition stack_owns `{logrelG Σ} := prestack_owns stack_name.

  Lemma stack_mapstos_agree l v w : l ↦ˢᵗᵏ v -∗ l ↦ˢᵗᵏ w -∗ ⌜v = w⌝.
  Proof. apply prestack_mapstos_agree. Qed.

  Lemma stack_owns_alloc `{logrelG Σ} h l v :
    stack_owns h ∗ l ↦ᵢ v ==∗ stack_owns (<[l := v]> h) ∗ l ↦ˢᵗᵏ v.
  Proof. apply prestack_owns_alloc. Qed.

  Lemma stack_owns_open_close `{logrelG Σ} h l v :
    stack_owns h -∗ l ↦ˢᵗᵏ v -∗ l ↦ᵢ v ∗ (l ↦ᵢ v -∗ stack_owns h).
  Proof. apply prestack_owns_open_close. Qed.

  Lemma stack_owns_later_open_close `{logrelG Σ} h l v :
    ▷ stack_owns h -∗ l ↦ˢᵗᵏ v -∗ ▷ (l ↦ᵢ v ∗ (l ↦ᵢ v -∗ stack_owns h)).
  Proof. apply prestack_owns_later_open_close. Qed.

  Definition StackLink Q v := preStackLink stack_name Q v.
  Lemma StackLink_unfold Q v :
    StackLink Q v ≡ (∃ (l : loc) w,
      ⌜v.1 = # l⌝ ∗ l ↦ˢᵗᵏ w ∗
      ((⌜w = InjLV #()⌝ ∧ ⌜v.2 = FoldV (InjLV #())⌝) ∨
      (∃ y1 z1 y2 z2, ⌜w = InjRV (PairV y1 (FoldV z1))⌝
                      ∗ ⌜v.2 = FoldV (InjRV (PairV y2 z2))⌝
                      ∗ Q (y1, y2) ∗ ▷ StackLink Q (z1, z2))))%I.
  Proof. by rewrite /StackLink preStackLink_unfold. Qed.

  Global Instance StackLink_persistent (Q : D) v `{∀ vw, PersistentP (Q vw)} :
    PersistentP (StackLink Q v).
  Proof. apply _. Qed.
  Global Opaque StackLink.

End Rules.
