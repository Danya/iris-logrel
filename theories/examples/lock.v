From iris.proofmode Require Import tactics.
From iris_logrel Require Export logrel.

Definition newlock : val := λ: <>, ref #false.
Definition acquire : val := rec: "acquire" "x" :=
  if: CAS "x" #false #true
  then #()
  else "acquire" "x".

Definition release : val := λ: "x", "x" <- #false.
Definition with_lock : val := λ: "e" "l" "x",
  acquire "l";;
  let: "v" := "e" "x" in
  release "l";; "v".

Definition LockType := Tref TBool.

Hint Unfold LockType : typeable.

Lemma newlock_type Γ : typed Γ newlock (TArrow TUnit LockType).
Proof. solve_typed. Qed.

Hint Resolve newlock_type : typeable.

Lemma acquire_type Γ : typed Γ acquire (TArrow LockType TUnit).
Proof. solve_typed. Qed.

Hint Resolve acquire_type : typeable.

Lemma release_type Γ : typed Γ release (TArrow LockType TUnit).
Proof. solve_typed. Qed.

Hint Resolve release_type : typeable.

Lemma with_lock_type Γ τ τ' :
  typed Γ with_lock (TArrow (TArrow τ τ') (TArrow LockType (TArrow τ τ'))).
Proof. solve_typed. Qed.

Hint Resolve with_lock_type : typeable.

Section proof.
  Context `{logrelG Σ}.
  Variable (E1 E2 : coPset).
  Variable (Δ : list (prodC valC valC -n> iProp Σ)).

  Lemma steps_newlock ρ j K
    (Hcl : nclose specN ⊆ E1) :
    spec_ctx ρ -∗ j ⤇ fill K (newlock #())
    ={E1}=∗ ∃ l : loc, j ⤇ fill K (of_val #l) ∗ l ↦ₛ #false.
  Proof.
    iIntros "#Hspec Hj".
    unfold newlock. unlock.
    tp_rec j. simpl.
    tp_alloc j as l "Hl". tp_normalise j.
    by iExists _; iFrame.
  Qed.

  Lemma bin_log_related_newlock_r Γ K t τ 
    (Hcl : nclose specN ⊆ E1) :
    (∀ l : loc, l ↦ₛ #false -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K #l : τ) -∗
    {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (newlock #()): τ.
  Proof.
    iIntros "Hlog".
    unfold newlock. unlock.
    rel_rec_r.
    rel_alloc_r as l "Hl".
    iApply ("Hlog" with "Hl").
  Qed.

  Lemma bin_log_related_newlock_l Γ K t τ :
    (∀ l : loc, l ↦ᵢ #false -∗ {E1,E1;Δ;Γ} ⊨ fill K #l ≤log≤ t : τ) -∗
    {E1,E1;Δ;Γ} ⊨ fill K (newlock #()) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    unfold newlock. unlock.
    rel_rec_l.
    rel_alloc_l as l "Hl".
    iApply ("Hlog" with "Hl").
  Qed.

  Global Opaque newlock.
  
  Transparent acquire. 
  Lemma steps_acquire ρ j K l
    (Hcl : nclose specN ⊆ E1) :
    spec_ctx ρ -∗ l ↦ₛ #false -∗ j ⤇ fill K (acquire #l)
    ={E1}=∗ j ⤇ fill K (Lit Unit) ∗ l ↦ₛ #true.
  Proof.
    iIntros "#Hspec Hl Hj". 
    unfold acquire. unlock.
    tp_rec j.
    tp_cas_suc j. simpl.
    tp_if_true j.
    by iFrame.
  Qed.
  
  Lemma bin_log_related_acquire_r Γ K l t τ
    (Hcl : nclose specN ⊆ E1) :
    l ↦ₛ #false -∗
    (l ↦ₛ #true -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Lit Unit) : τ) -∗
    {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (acquire #l) : τ.
  Proof.
    iIntros "Hl Hlog".
    unfold acquire. unlock.
    rel_rec_r.
    rel_cas_suc_r. simpl.
    rel_if_r.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_acquire_suc_l Γ K l t τ :
    l ↦ᵢ #false -∗
    (l ↦ᵢ #true -∗ {E1,E1;Δ;Γ} ⊨ fill K (#()) ≤log≤ t : τ) -∗
    {E1,E1;Δ;Γ} ⊨ fill K (acquire #l) ≤log≤ t : τ.
  Proof.
    iIntros "Hl Hlog".
    unfold acquire. unlock.
    rel_rec_l.
    rel_cas_l_atomic. iModIntro.
    iExists _; iFrame "Hl".
    iSplitR.
    { iIntros (Hfoo). congruence. }
    iIntros (_). iNext. iIntros "Hl".
    rel_if_l.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_acquire_fail_l Γ K l t τ :
    l ↦ᵢ #true -∗
    (l ↦ᵢ #false -∗ {E1,E1;Δ;Γ} ⊨ fill K (acquire #l) ≤log≤ t : τ) -∗
    {E1,E1;Δ;Γ} ⊨ fill K (acquire #l) ≤log≤ t : τ.
  Proof.
    iIntros "Hl Hlog".
    iLöb as "IH".
    unfold acquire. unlock.
    rel_rec_l.
    rel_cas_l_atomic. iModIntro.
    iExists _; iFrame "Hl".
    iSplitL; last first.
    { iIntros (Hfoo). congruence. }
    iIntros (_). iNext. iIntros "Hl".
    rel_if_l.
    iApply ("IH" with "Hl Hlog").
  Qed.

  Global Opaque acquire.

  Transparent release.
  Lemma steps_release ρ j K l (b : bool)
    (Hcl : nclose specN ⊆ E1) :
    spec_ctx ρ -∗ l ↦ₛ #b -∗ j ⤇ fill K (release #l)
    ={E1}=∗ j ⤇ fill K #() ∗ l ↦ₛ #false.
  Proof.
    iIntros "#Hspec Hl Hj". unfold release. unlock.
    tp_rec j.
    tp_store j.
    by iFrame.
  Qed.

  Lemma bin_log_related_release_r Γ K l t τ (b : bool)
    (Hcl : nclose specN ⊆ E1) :
    l ↦ₛ #b -∗
    (l ↦ₛ #false -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (Lit Unit) : τ) -∗
    {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (release #l) : τ.
  Proof.
    iIntros "Hl Hlog".
    unfold release. unlock.
    rel_rec_r.
    rel_store_r.
    by iApply "Hlog".
  Qed.

  Global Opaque release.

  Lemma steps_with_lock ρ j K e ev l P Q v w:
    to_val e = Some ev →
    (∀ K', spec_ctx ρ -∗ P -∗ j ⤇ fill K' (App e (of_val w))
            ={E1}=∗ j ⤇ fill K' (of_val v) ∗ Q) →
    (nclose specN ⊆ E1) →
    spec_ctx ρ -∗ P -∗ l ↦ₛ #false -∗
    j ⤇ fill K (with_lock e #l w)
    ={E1}=∗ j ⤇ fill K (of_val v) ∗ Q ∗ l ↦ₛ #false.
  Proof.
    iIntros (Hev Hpf ?) "#Hspec HP Hl Hj".
    unfold with_lock. unlock.
    tp_rec j.
    tp_rec j.
    tp_rec j.
    tp_bind j (App acquire (# l)).
    tp_apply j steps_acquire with "Hl" as "Hl".
    tp_rec j.
    tp_bind j (App e _).
    iMod (Hpf with "Hspec HP Hj") as "[Hj HQ]". 
    tp_normalise j.
    tp_rec j; eauto using to_of_val.
    tp_bind j (App release _).    
    tp_apply j steps_release with "Hl" as "Hl".
    tp_normalise j.
    tp_rec j.
    by iFrame.
  Qed.
  
  Lemma bin_log_related_with_lock_r Γ K Q e ev ew cl v w l t τ :
    (to_val e = Some cl) →
    (to_val ev = Some v) →
    (to_val ew = Some w) →
    (nclose specN ⊆ E1) →
    (∀ K, (Q -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K ev : τ) -∗
        {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (App e ew) : τ) -∗
    l ↦ₛ #false -∗
    (Q -∗ l ↦ₛ #false -∗ {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K ev : τ) -∗
    {E1,E2;Δ;Γ} ⊨ t ≤log≤ fill K (with_lock e #l ew) : τ.
  Proof.
    iIntros (????) "HA Hl Hlog".
    rel_bind_r (with_lock e).
    unfold with_lock. unlock. (*TODO: unlock here needed *)
    iApply (bin_log_related_rec_r); eauto. simpl_subst.
    rel_bind_r (App _ (# l)).
    iApply (bin_log_related_rec_r); eauto. simpl_subst.
    iApply (bin_log_related_rec_r); eauto. simpl_subst.
    rel_bind_r (App acquire (# l)).
    iApply (bin_log_related_acquire_r Γ (_ :: K) l with "Hl"); auto.
    iIntros "Hl". simpl.
    iApply (bin_log_related_rec_r); eauto. simpl_subst/=.
    rel_bind_r (App e ew).
    iApply "HA". iIntros "HQ". simpl.
    iApply (bin_log_related_rec_r); eauto. simpl_subst.
    rel_bind_r (App release _).
    iApply (bin_log_related_release_r with "Hl"); eauto.
    iIntros "Hl". simpl.
    iApply (bin_log_related_rec_r); eauto. simpl_subst.
    iApply ("Hlog" with "HQ Hl").
  Qed.

End proof.
