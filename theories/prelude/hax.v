(* the contents of this file sould belong elsewhere *)
From iris_logrel.F_mu_ref_conc Require Import lang.

Definition lamsubst (e : expr) (v : val) : expr :=
  match e with
  | Rec (BNamed f) x e' => lang.subst f e (subst' x (of_val v) e')
  | Rec BAnon x e' => subst' x (of_val v) e'
  | _ => e
  end.

Notation "e $/ v" := (lamsubst e%E v%V) (at level 71, left associativity) : expr_scope.
(* ^ this should be left associative at a level lower than that of `bin_log_related` *)
